Skeptik
===================

Skeptik is a secure, network architecture that provides end-to-end encryption without the need of a certificate authority.


Architecture
-------------

A Skeptik network architecture operates in a centralized fashion: a server manages communication between clients.

A connection between a server and a client is established once the Skeptik handshake between the two entities has been completed. The handshake details a private key to be used by both entities for symmetric encryption and decryption, and also a randomly generated numerical ID that the client will be associated with. 

This randomly generated ID is given to all clients as replacement to utilizing their IP addresses as an identifier - which may open clients for malicious attacks. 

For a client to send message to another, it must know the ID of the receiver. The server also has a unique ID associated with it as to comply with this standard. It is given during the Skeptik handshake.

Communication between two clients follows a two-step encryption and decryption process:
1.) The sender encrypts their message using their private key and sends it to the server. The server then decrypts this message.
2.) The server encrypts this message using the receiver's private key and sends it to the receiver. The receiver then decrypts this message using their private key. 


