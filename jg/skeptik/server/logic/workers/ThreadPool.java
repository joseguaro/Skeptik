package jg.skeptik.server.logic.workers;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * A fixed-sized thread pool that consists of worker threads
 * 
 * @author Jose Guaro
 *
 */
public class ThreadPool {
		
	private Worker[] workers;
	private ConcurrentLinkedQueue<Task<?>> tasks;
	
	private boolean workersInitialized;
	
	/**
	 * Constructs a ThreadPool with a fixed size
	 * @param poolSize - the size of this ThreadPool
	 */
	public ThreadPool(int poolSize){
		workers = new Worker[poolSize];
		tasks = new ConcurrentLinkedQueue<>();
	}
  
	/**
	 * Initializes the worker threads of this thread pool.
	 * 
	 * This method must be called prior to submitting a Task
	 */
	public void initWorkers() {
		if (workersInitialized == false) {
			workersInitialized = true;
			for(int i = 0; i < workers.length ; i++){
				Worker worker = new Worker(tasks, "WORKER-"+i);
				workers[i] = worker;
				worker.start();
			}
		}
	}
	
	/**
	 * Submits a Task to be executed by an available worker in this 
	 * thread pool. 
	 * Note: There is no guarantee that such a Task will be executed immediately
	 * @param task - the Task to be executed
	 * @return true - if this Task was submitted immediately. False if else
	 */
	public boolean submitTask(Task<?> task){
		if (workersInitialized) {
			return tasks.add(task);
		}
		else {
			throw new IllegalStateException("Thread pool hasn't been initialized");
		}
	}
	
	/**
	 * Stops all workers in this thread pool from accepting new Tasks to execute
	 */
	public void shutdown(){
	  
		for(Worker worker: workers){
			worker.stopWork();
		}
	}
	
	/**
	 * Stops all worker in this thread pool from accepting new Tasks to execute, and 
	 * also interrupts the ongoing Tasks each Worker is executing - including awaiting 
	 * for a new Task to execute.
	 */
	public void shutdownNow(){
		for(Worker worker : workers){
			worker.stopWorkNow();
		}
	}
	
	/**
	 * Returns the amount of Workers in this thread pool
	 * @return the amount of Workers in this thread pool
	 */
	public int workerAmnt(){
		return workers.length;
	}
	
	
}
