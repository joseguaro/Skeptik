package jg.skeptik.server.logic.workers;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class Task<V> extends FutureTask<V>{
	
	public enum TaskType{
		DEDICATED_LINE, SERVER_REQUEST, INTERNAL_PROCESS, OTHER;
	}
	
	private final boolean taskHasReturn;
	private final TaskType taskType;
	
	private String taskName;

	public Task(Callable<V> callable, TaskType taskType) {
		super(callable);
		this.taskType = taskType;
		taskHasReturn = true;
	}
	
	public Task(Runnable runnable, TaskType taskType){
		super(runnable, null);
		this.taskType = taskType;
		taskHasReturn = false;
	}
	
	public void setName(String taskName){
		this.taskName = taskName;
	}
	
	public String getName(){
		return taskName;
	}
	
	public boolean taskHasReturn(){
		return taskHasReturn;
	}

	public TaskType getTaskType(){
		return taskType;
	}
}
