package jg.skeptik.server.logic.workers;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * A worker executes tasks from a task queue
 * @author Jose Guaro
 *
 */
public class Worker extends Thread{
	
	private volatile boolean stopWork;
	private ConcurrentLinkedQueue<Task<?>> taskQueue;
	
	/**
	 * Constructs a Worker with a task queue to get tasks from
	 * and a String name to identify this worker
	 * @param taskQueue
	 * @param workerName
	 */
	public Worker(ConcurrentLinkedQueue<Task<?>> taskQueue, String workerName){
		super(workerName);
		this.taskQueue = taskQueue;
		stopWork = false;
	}
	
	/**
	 * Starts this worker. 
	 * 
	 * The worker will wait for task to become available in the queue. It will stop
	 * when stopWork() or stopWorkNow() is called.
	 * 
	 * Note: Calling stopWorkNow() will immediately stop this worker by calling 
	 *       Thread.interrupt(). 
	 */
	public void run(){
	  while (stopWork != true) {
	    while (stopWork != true && taskQueue.isEmpty());
	    Task<?> task = taskQueue.poll();
	    if (task != null) {
	      if (task.getName() != null) {
	        setName(getName()+" : RUNNING -> "+task.getName());
	      }
	      task.run();
	    }
	  }

	}
	
	/**
	 * Stops this worker from accepting new Tasks to execute
	 */
	public void stopWork(){
		stopWork = true;
	}
	
	/**
	 * Stops this worker from accepting new Tasks to execute
	 * and signals this worker to stop the current Task it is 
	 * executing
	 */
	public void stopWorkNow(){
		stopWork();
		this.interrupt();
	}
	
}
