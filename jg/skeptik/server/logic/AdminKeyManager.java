package jg.skeptik.server.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.SignedObject;


/**
 * Manages the keys used to verify the legitimacy of Commands
 * 
 * Commands legitimized using a private DSA key created by the 
 * Server at start up
 * 
 * When a command is received, it is verified using the public DSA key
 * that corresponds to the private key.
 * 
 * @author Jose Guaro
 *
 */
public final class AdminKeyManager {
  
  private KeyPair keyPair;
  
  /**
   * Constructs an AdminKeyManager with a newly generated DSA keypair
   * @throws NoSuchAlgorithmException
   */
  public AdminKeyManager() throws NoSuchAlgorithmException {
    keyPair = generateKey();
  }
  
  /**
   * Constructs an AdminKeyManager that reads a DSA key pair from a file
   * @param path - the File object containing the path to the file
   * @throws IllegalArgumentException - if file being red is not a valid key pair, or is incomplete
   * @throws IOException - if an I/O error occurs
   */
  public AdminKeyManager(File path) throws IOException  {
    try {
      keyPair = readKeyPair(path);
    } catch (ClassNotFoundException e) {
      throw new IllegalArgumentException("File not a valid key pair, or is incomplete");
    }
  }
  
  private KeyPair readKeyPair(File path) throws IOException, ClassNotFoundException {
    if (path.isFile()) {
      
      FileInputStream inputStream = new FileInputStream(path);
      ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
      
      KeyPair pair = (KeyPair) objectInputStream.readObject();
      objectInputStream.close();
      return pair;
    }
    throw new IllegalArgumentException("File is a directory!");
  }
  
  private KeyPair generateKey() throws NoSuchAlgorithmException {
    KeyPairGenerator generator = KeyPairGenerator.getInstance("DSA");
    return generator.generateKeyPair(); 
  }
  
  /**
   * Writes the key pair to a file.
   * @param path - the directory to where the key pair will be written in
   * @throws IOException - if an I/O error occurs
   */
  public void writeKeyToFile(String path) throws IOException{
    File file = new File(path);
    if (file.isDirectory()) {
      File keyPath = new File(path+"adminKey.skey");
      
      FileOutputStream fileOut = new FileOutputStream(keyPath);
      ObjectOutputStream outputStream = new ObjectOutputStream(fileOut);
      
      outputStream.writeObject(keyPair);
      
      fileOut.flush();
      
      outputStream.close();
    }
    else {
      throw new IllegalArgumentException("path must be a directory");
    }
  }
  
  /**
   * Generates a new keypair
   * @param path - the directory at where the keypair will be written in
   * @throws NoSuchAlgorithmException - if the host machine doesn't support DSA
   * @throws IOException - if an I/O error occurs
   */
  public void regenerateKey(String path) throws NoSuchAlgorithmException, IOException {
    generateKey();
    writeKeyToFile(path);
  }
  
  /**
   * Verfies the legitimacy of the given SignedObject.
   * 
   * It is intended that the SignedObject contains a Command
   * 
   * @param signedObject - the SignedObject to verify
   * @return true if signedObject is legit, false if else.
   */
  public boolean verifyCommand(SignedObject signedObject) {
    try {
      Signature signature = Signature.getInstance(keyPair.getPublic().getAlgorithm());
      return signedObject.verify(keyPair.getPublic(), signature);
    } catch (InvalidKeyException e) {
      e.printStackTrace();
    } catch (SignatureException e) {
      e.printStackTrace();
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return false;
  }
  
  /**
   * Returns the private key of the KeyPair
   * @return the private key of the KeyPair
   */
  public PrivateKey privateKey() {
    return keyPair.getPrivate();
  }
  
}
