package jg.skeptik.server.logic;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.security.NoSuchAlgorithmException;

import jg.skeptik.common.ClientID;
import jg.skeptik.server.config.BasicConfigVars;
import jg.skeptik.server.config.ServerConfigs;
import jg.skeptik.server.logic.engine.ActionProcessor;
import jg.skeptik.server.logic.engine.Engine;

/**
 * Manages the initialization and execution 
 * of a Skeptik server
 * 
 * @author Jose Guaro
 */
public class SkeptikServer {
  
  private final int port;
  
  private final ClientID serverID;
  private final ServerSocketChannel channel;
  private final ServerConfigs configs;
  
  private SocketAddress socketAddress;
  private AdminKeyManager adminKeyManager;
  private Selector selector;
  private boolean initialized;
  
  /**
   * Constructs a SkeptikServer
   * @param port - the port at which this server will be bound to
   * @param configs - the configurations this server will run on
   * @throws IOException - if an IO error occurs
   */
  public SkeptikServer(int port, ServerConfigs configs) throws IOException {
    this.serverID = ClientID.getServerID((String) configs.getValue(BasicConfigVars.SERVER_ID_USERNAME));
    this.channel = ServerSocketChannel.open();
    this.configs = configs;
    this.port = port;
  }
  
  /**
   * Initializes this server for IO operations
   * @return the Selector to be used during IO operation
   * @throws NoSuchAlgorithmException 
   * @throws ClassNotFoundException 
   */
  public Selector initialize(File keyPath) throws IOException, NoSuchAlgorithmException, ClassNotFoundException{
    if (!initialized) {
      channel.bind(new InetSocketAddress(9999), 10);
      channel.configureBlocking(false);
      channel.socket().setReuseAddress(true);
      channel.socket().setSoTimeout((int) configs.getValue(BasicConfigVars.CON_TIME_OUT));
      
      this.socketAddress = channel.getLocalAddress();
      
      if (keyPath != null) {
        adminKeyManager = new AdminKeyManager(keyPath);
      }
      else {
        adminKeyManager = new AdminKeyManager();
        adminKeyManager.writeKeyToFile((String) configs.getValue(BasicConfigVars.ADMIN_KEY_PATH));
      }
      
      
      selector = Selector.open();
      channel.register(selector, SelectionKey.OP_ACCEPT);
      return selector;
    }
    return selector;
  }
  
  /**
   * Starts this server
   * @param engine - the Engine that will run this Skeptik Server
   * @param connectionManager - the AbstractConnectionManager that will handle client communications
   *                            and acceptance
   * @param processor - the ActionProcessor that will process Actions for the Engine
   */
  public void startServer(Engine engine, ConnectionManager connectionManager, ActionProcessor processor) {
    engine.initialize(connectionManager);
    engine.start(processor);
  }
  
  /**
   * Returns the AdminKeyManager of this SkeptikServer
   * @return the AdminKeyManager of this SkeptikServer
   */
  public AdminKeyManager getAdminManager() {
    return adminKeyManager;
  }
  
  /**
   * Returns the SocketAddress of this SkeptikServer
   * 
   * Note: Since this SocketAddress returns the address the ServerSocketChannel is bound to,
   *       it will return null if initialize() has not been called
   * 
   * @return the SocketAddress of this SkeptikServer
   */
  public SocketAddress getServerAddress() {
    return socketAddress;
  }
  
  /**
   * Returns this server's ClientID
   * @return this server's ClientID
   */
  public ClientID serverId() {
    return serverID;
  }
  
  /**
   * Returns the ServerConfigs of this server
   * @return the ServerConfigs of this server
   */
  public ServerConfigs getConfigs() {
    return configs;
  }
  
  /**
   * Shutsdown this server
   * @throws IOException - if an IO error occurs
   */
  public void shutdown() throws IOException {
    selector.close();
    channel.close();
        
    print("SKP SERVER: Selector and ServerSocketChannel closed!!!");
  }
  
  /**
   * Handles the given exception.
   * @param exception - the Exception to be handled
   */
  public void handleException(Exception exception) {
    exception.printStackTrace();
    System.exit(-1);
  }
  
  /**
   * Prints the given string message to this server's standard output
   * @param mess - the String to print
   */
  public static void print(String mess) {
    System.out.println(mess);
  }
  
  /**
   * Prints the given string message to this server's standard error output
   * @param mess - the String to print
   */
  public static void printError(String mess) {
    System.err.println(mess);
  }
}
