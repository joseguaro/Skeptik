package jg.skeptik.server.logic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;


import jg.skeptik.common.ClientID;

/**
 * A basic implementation of PipeDatabase.
 * 
 * @author Jose Guaro
 *
 */
public class BasicDatabase implements PipeDatabase{
	
	private final Map<Integer, Pipe> clientMap;
	private final Queue<ClientID> clientIDs;
	
	/**
	 * Constructs a BasicDatabase with an initial capacity
	 * for 10 clients
	 */
	public BasicDatabase(){
		clientMap = new ConcurrentHashMap<>( 10,  ((float)2)/3);
		clientIDs = new ConcurrentLinkedQueue<>();
	}
	
	/**
	 * Adds a Pipe to this database
	 */
	@Override
	public void addPipe(Pipe pipe){
		clientMap.put(pipe.getClientID().getID(), pipe);
		clientIDs.offer(pipe.getClientID());
	}
	
	/**
	 * Terminates the connection with the given Client and
	 * removes the Client from the database.
	 * 
	 * The Pipe no longer allows for IO operations, and will
	 * throw an IO exception of some kind at an attempt
	 * at reading or writing 
	 * 
	 * @param pipe - the Pipe to terminate
	 * @throws IOException - if an IO error.
	 */
	private void terminateConnection(Pipe pipe) throws IOException{
		pipe.closePipe();
		clientMap.remove(pipe.getClientID().getID());
		clientIDs.remove(pipe.getClientID());
	}
	
	/**
	 * Removes the Client with the given ClientID from this server's database 
	 * and terminate the connection with the Client.
	 * @param id - the ClientID of the Client to remove
	 * @throws IOException
	 */
	@Override
	public boolean removeClient(ClientID id) throws IOException{
		Pipe pipe = clientMap.remove(id.getID());
		if (pipe != null) {
			terminateConnection(pipe);
			return true;
		}
		return false;
	}
	
	/**
	 * Removes the Client with the given numerical ID from this server's database 
	 * and terminate the connection with the Client.
	 * @param id - the numerical ID of the Client to remove
	 * @throws IOException
	 */
	@Override
	public boolean removeClient(int id) throws IOException{
		Pipe pipe = clientMap.remove(id);
		if (pipe != null) {
			terminateConnection(pipe);
			return true;
		}
		return false;
	}
	
	/**
	 * Returns the Pipe with the given ClientID
	 * @return the Pipe with the given ClientID or null
	 *         if it doesn't exist 
	 */
	@Override
	public Pipe getClient(ClientID id){
		return clientMap.get(id.getID());
	}
	
	/**
	 * Returns the Pipe with the given numerical ClientID
   * @return the Pipe with the given numerical ClientID or null
   *         if it doesn't exist 
	 */
	@Override
	public Pipe getClient(int id){
		return clientMap.get(id);
	}
	
	/**
	 * Returns true if the numerical id corresponds with Pipe in this database
	 * @return true if the numerical id corresponds with Pipe in this database
	 */
	@Override
	public boolean clientPresent(int id){
		return clientMap.containsKey(id);
	}
	
	/**
   * Returns true if the ClientID corresponds with Pipe in this database
   * @return true if the ClientID corresponds with Pipe in this database
   */
	@Override
	public boolean clientPresent(ClientID id){
		return clientMap.containsKey(id.getID());
	}
	
	/**
   * Returns a list of all ClientIDs registered in this database
   * @return a list of all ClientIDs registered in this database
   */
	@Override
  public List<ClientID> idList() {
    return new ArrayList<>(clientIDs);
  }

  /**
	 * Returns the amount of clients in this database
	 * @return the amount of clients in this database
	 */
	@Override
	public int size(){
		return clientMap.size();
	}
	
}
