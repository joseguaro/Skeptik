package jg.skeptik.server.logic;

import java.io.IOException;
import java.io.Serializable;
import java.net.SocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SignedObject;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import javax.crypto.KeyGenerator;
import javax.crypto.SealedObject;

import jg.skeptik.common.ClientID;
import jg.skeptik.common.ConnectionInfo;
import jg.skeptik.common.admin.Command;
import jg.skeptik.common.parcel.Parcel;
import jg.skeptik.common.parcel.ParcelContent;
import jg.skeptik.common.request.ServerRequest;
import jg.skeptik.server.config.BasicConfigVars;
import jg.skeptik.server.logic.engine.AbstractSkeptikEngine;
import jg.skeptik.server.logic.engine.Action;
import jg.skeptik.server.logic.engine.Action.Code;

/**
 * A ConnectionManager manages connections with clients.
 * 
 * More specifically, a ConnectionManager accepts new clients and
 * passes in any incoming Parcels from connected Clients.
 * 
 * It is ideal that the ConnectionManager runs on its own thread, while
 * passing Actions to an Engine running concurrently. 
 * 
 * @author Jose Guaro
 *
 */
public class ConnectionManager implements ConnectionManagerOptions{
  protected final PipeDatabase database;
	
  /**
   * Set of String IP addresses that are banned from connecting with this ConnectionManager
   */
	protected final Set<String> bannedIPAddresses;
	
	
	protected final SkeptikServer server;
	protected final AbstractSkeptikEngine engine;
	protected final Selector selector; 
	
	/**
	 * Set to true if client acceptance/parcel exchange has occured
	 */
	protected boolean hasStartedComms;
	
	protected volatile boolean stopComms;
	
  /**
   * Set to true if client acceptance and parcel exchange has stopped
   */
	private volatile boolean reachedCycleEnd;
	
	protected volatile boolean stopParcelPassing;
	protected volatile boolean stopRequests;
	protected volatile boolean stopAccepts;
	
	/**
	 * AES key generator used when accepting new clients
	 */
	protected KeyGenerator keyGen; //KeyGenerator to generate AES keys

	/**
	 * Constructs a ConnectionManager for a SkeptikServer
	 * 
	 * @param server
	 * @param selector
	 * @throws NoSuchAlgorithmException
	 */
	public ConnectionManager(SkeptikServer server, AbstractSkeptikEngine engine, Selector selector) {
	  this.server = server;
		this.engine = engine;
		this.selector = selector;
		this.database = engine.database();
		
		this.bannedIPAddresses = Collections.synchronizedSet(new TreeSet<>());
		
		try {
      keyGen = KeyGenerator.getInstance("AES");
      keyGen.init((int) server.getConfigs().getValue(BasicConfigVars.AES_KEY_SIZE));
    } catch (NoSuchAlgorithmException e) {
      server.handleException(e);
    }
	}
	
	/**
	 * Starts the acceptance of clients and reads sent Parcels from Clients
	 */
	public final void startComms() {
	  if (hasStartedComms == false) {
	    
	    //Amount of time, in miliseconds, that is given for select() when polling for
	    //ready clients
	    int selectorTimeout = (int) server.getConfigs().getValue(BasicConfigVars.SELECTOR_TIME_OUT);
	    
      hasStartedComms = true;
      
      SkeptikServer.print("SKP SERVER: ConnectionManager has started!");
      while (stopComms == false) {
        try {
          int selected = selector.select(selectorTimeout);
          if (selected > 0) {
            Iterator<SelectionKey> selectedKeys = selector.selectedKeys().iterator();
            
            while (selectedKeys.hasNext()) {
              SelectionKey selectionKey = selectedKeys.next();
              selectedKeys.remove();
              
              if (selectionKey.isAcceptable() && stopAccepts == false) {
                ServerSocketChannel serverSocket = (ServerSocketChannel) selectionKey.channel();
                SocketChannel connecting = serverSocket.accept();
                
                if (connecting != null && bannedIPAddresses.contains(connecting.getRemoteAddress().toString()) == false) {
                  connecting.configureBlocking(false);
                                    
                  SelectionKey connectingKey = connecting.register(selector, SelectionKey.OP_READ);
                  
                  ClientID clientID = new ClientID();
                  Pipe pipe = new Pipe(clientID, connecting, connectingKey, keyGen.generateKey());
                  database.addPipe(pipe);
                  connectingKey.attach(pipe);
                  
                  String goodByeString = (String) server.getConfigs().getValue(BasicConfigVars.SERVER_BYE);
                  ConnectionInfo connectionInfo = new ConnectionInfo(clientID, server.serverId(), goodByeString);
                  pipe.write(connectionInfo);
                                    
                  SkeptikServer.print("SKP SERVER: Client "+clientID.getID()+" has connected....");
                }
              }
              else if (selectionKey.isReadable()) {
                Pipe pipe = (Pipe) selectionKey.attachment();
                try {
                  Parcel parcel = (Parcel) pipe.read();
                  
                  if (parcel.getContentType() == ParcelContent.PUBLIC_KEY) {
                    PublicKey publicKey = (PublicKey) parcel.getObject();
                    Action<PublicKey> action = new Action<>(Code.PUBLIC_KEY, publicKey, pipe, parcel.getAddress());
                    engine.submitAction(action);
                  }
                  else {
                    SealedObject sealedObject = (SealedObject) parcel.getObject();
                    Serializable decrypted = null;
                    
                    try {
                      decrypted = (Serializable) sealedObject.getObject(pipe.getAesSecretKey());
                    } catch (Exception e) {
                      SkeptikServer.print("SKP SERVER: Error decrypting message from "+pipe.getClientID());
                      e.printStackTrace();
                    } 
                    
                    if (parcel.getContentType() == ParcelContent.MESSAGE) {
                      Action<Serializable> action = new Action<>(Code.PARCEL, decrypted, pipe, parcel.getAddress());
                      engine.submitAction(action);
                      
                      //SkeptikServer.print("SKP SERVER: Client "+pipe.getClientID()+" sent a MESSAGE");
                    }
                    else if (parcel.getContentType() == ParcelContent.SERVER_REQUEST) {
                      ServerRequest request = (ServerRequest) decrypted;
                      Action<ServerRequest> action = new Action<>(Code.REQUEST, request, pipe, parcel.getAddress());
                      engine.submitAction(action);
                      
                      SkeptikServer.print("SKP SERVER: Client "+pipe.getClientID()+" issued a REQUEST");
                    }
                    else if (parcel.getContentType() == ParcelContent.COMMAND) {
                      SignedObject potential = (SignedObject) decrypted;
                      if (server.getAdminManager().verifyCommand(potential)) {
                        Command command = (Command) potential.getObject();
                        Action<Command> action = new Action<>(Code.COMMAND, command , pipe, parcel.getAddress());
                        engine.submitAction(action);
                        
                        SkeptikServer.print("SKP SERVER: Client "+pipe.getClientID()+" issued a legit command");
                      }
                      else {
                        SkeptikServer.print("SKP SERVER: Client "+pipe.getClientID()+" issued a illegitimate command!!");
                      }
                      
                    }
                  }
                } catch (ClassNotFoundException e) {
                  SkeptikServer.print("SKP SERVER: Casting error at Parcel recieving");
                  e.printStackTrace();
                } catch (IOException e) {
                  SkeptikServer.print("SKP SERVER: IO Error at ConnectionManager with Client: "+pipe.getClientID());
                  pipe.closePipe();
                }
              }
            }
          }
        } catch (IOException e) {
          SkeptikServer.print("SKP SERVER: IO Error at ConnectionManager");
          e.printStackTrace();
        }
      }
    }
	  
	  reachedCycleEnd = true;
    SkeptikServer.print("SKP SERVER: ConnectionManager has STOPPED!");
	}
	
	/**
	 * Blocks the given IP address from connecting to the server
	 * @param ip - the String representation of the IP Address
	 */
	public void blockAddress(String ip) {
	  bannedIPAddresses.add(ip);
	}
	
	/**
   * Blocks the given IP address from connecting to the server
   * @param ip - the SocketAddress that holds the IP address to be blocked
   */
	public void blockAddress(SocketAddress ip) {
    bannedIPAddresses.add(ip.toString());
  }
	
	/**
	 * Stops the acceptance of new clients
	 * @param bool - true if client acceptance should be stopped, false if else
	 */
	@Override
  public void stopAccepts(boolean bool) {
    this.stopAccepts = bool;
  }
  
  /**
   * Stops the reading of Parcels, and Parcel exchanges
   * @param bool - true if parcel reading and exchange should be stopped, false if else
   */
	@Override
  public void stopParcelPassing(boolean bool) {
    this.stopParcelPassing = bool;
  }
  
  /**
   * Stops the fulfillment of ServerRequests
   * Note: This stops future fulfillment of ServerRequests - once made after a call
   *       to this method - and doesn't stop ServerRequests currently being fulfilled
   * @param bool - true if client acceptance should be stopped, false if else
   */
	@Override
  public void stopRequests(boolean bool) {
    this.stopRequests = bool;
  }
  
  /**
   * Returns true if this ConnectionManager 
   * has been set to stop accepting clients, false if else.
   * @return true if this ConnectionManager 
   *         has been set to stop accepting clients, false if else.
   */
	@Override
  public boolean hasStoppedAccepts() {
    return stopAccepts;
  }
  
	/**
   * Returns true if this ConnectionManager 
   * has been set to stop parcel exchanges, false if else.
   * @return true if this ConnectionManager 
   *         has been set to stop parcel exchanges, false if else
   */
	@Override
  public boolean hasStoppedParcelPassing() {
    return stopParcelPassing;
  }
  
	/**
	 * Returns true if this ConnectionManager has been set to 
	 * stop the fulfillment of ServerRequests, false if else.
	 * 
	 * @return true if this ConnectionManager has been set to stop the 
	 *         fulfillment of ServerRequests, false if else.
	 */
	@Override
  public boolean hasStoppedRequests() {
    return stopRequests;
  }
  
	/**
	 * Returns true if this ConnectionManager 
	 * has stopped communications, false if else.
	 * @return true if this ConnectionManager 
	 *         has stopped communications, false if else.
	 */
  public boolean hasStoppedComms() {
    return hasStartedComms && reachedCycleEnd;
  }
  
  /**
   * Returns the Set of Strings of blocked IP addresses
   * @return the Set of Strings of blocked IP addresses
   */
  public Set<String> blockedAddresses(){
    return bannedIPAddresses;
  }
		
	/**
	 * Stops the acceptance of clients and passing of Parcels
	 */
	public void stopComms() {
	  stopComms = true;
	  stopAccepts = true;
	  stopParcelPassing = true;
	  stopRequests = true;
	}
}
