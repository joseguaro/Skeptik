package jg.skeptik.server.logic;

import java.io.IOException;
import java.util.List;

import jg.skeptik.common.ClientID;

/**
 * An interface that lays out a Pipe database
 * 
 * @author Jose Guaro
 */
public interface PipeDatabase {
	
  /**
   * Adds a Pipe to this database
   */
	public void addPipe(Pipe pipe);
	
	/**
   * Removes the Client with the given ClientID from this server's database 
   * and terminate the connection with the Client.
   * @param id - the ClientID of the Client to remove
   * @throws IOException
   */
	public boolean removeClient(ClientID clientID) throws IOException;

	/**
   * Removes the Client with the given numerical ID from this server's database 
   * and terminate the connection with the Client.
   * @param id - the numerical ID of the Client to remove
   * @throws IOException
   */
  public boolean removeClient(int clientID) throws IOException;
	
  /**
   * Returns the Pipe with the given ClientID
   * @return the Pipe with the given ClientID or null
   *         if it doesn't exist 
   */
	public Pipe getClient(ClientID clientID);
	
	/**
   * Returns the Pipe with the given numerical ClientID
   * @return the Pipe with the given numerical ClientID or null
   *         if it doesn't exist 
   */
	public Pipe getClient(int clientID);
	
	/**
   * Returns true if the numerical id corresponds with Pipe in this database
   * @return true if the numerical id corresponds with Pipe in this database
   */
	public boolean clientPresent(int id);
	
	/**
   * Returns true if the ClientID corresponds with Pipe in this database
   * @return true if the ClientID corresponds with Pipe in this database
   */
	public boolean clientPresent(ClientID id);
	
	/**
   * Returns a list of all ClientIDs registered in this database
   * @return a list of all ClientIDs registered in this database
   */
	public List<ClientID> idList();
	
	 /**
   * Returns the amount of clients in this database
   * @return the amount of clients in this database
   */
	public int size();
	
}
