package jg.skeptik.server.logic;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

import javax.crypto.SecretKey;

import jg.skeptik.common.ClientID;
import jg.skeptik.common.ClientPhase;

/**
 * A representation of a client connected to the Skeptik server
 * 
 * A Pipe allows for the simplification of IO operations
 * between clients and the Skeptik server
 * 
 * @author Jose Guaro
 */
public class Pipe {
	
	private final ClientID clientID;
	private final SocketChannel socketChannel;
	private final SelectionKey selectionKey;
	private final SecretKey aesSecretKey;
	
	private final SocketAddress remoteAdd;
	
	private volatile ClientPhase phase;
	
	private volatile boolean closed = false;
	

	/**
	 * Constructs a Pipe.
	 * @param clientID - the ClientID to identify this pipe
	 * @param socket - the SocketChannel to this client
	 * @param selKey - the SelectionKey associated with Client's SocketChannel
	 * @param aesKey - the AES key used to encrypt messages to and from this client
	 * @throws IOException 
	 */
	public Pipe(ClientID clientID, SocketChannel socket, SelectionKey selKey, SecretKey aesKey) throws IOException {
		this.clientID = clientID;
		this.socketChannel = socket;
		this.selectionKey = selKey;
		this.aesSecretKey = aesKey;
		this.remoteAdd = socket.getRemoteAddress();
		
		phase = ClientPhase.PHASE_1;
	}
	
	/**
	 * Reads an Object from this Pipe's IO stream - through the backing SocketChannel.
	 * 
	 * Note: the a Skeptik server is non-blocking by design, so if there is no data
	 * in this Pipe's IO stream, a null value will be returned or an IOException 
	 * would be thrown
	 * 
	 * @return the Object in this pipe's IO stream.
	 * @throws ClassNotFoundException 
	 */
	public synchronized Object read() throws IOException, ClassNotFoundException{
		if (closed) {
			throw new IllegalStateException("Pipe is closed");
		} 
		else {
			ByteBuffer readBuffer = ByteBuffer.allocate(1);
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			
			while (socketChannel.read(readBuffer) > 0) {
				if(readBuffer.hasArray()){
					outputStream.write(readBuffer.array());
					readBuffer.clear();
				}
			}
			
			readBuffer.clear();
			if(outputStream.size() > 0){
				ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
				ObjectInputStream objectInput = new ObjectInputStream(inputStream);
				return objectInput.readObject();
			}
			return null;
		}
	}
	
	/**
	 * Writes an object to this pipe's IO stream
	 * @param object - the Object to write
	 * @throws IOException 
	 */
	public synchronized void write(Object object) throws IOException {
		if (closed) {
			throw new IllegalStateException("Pipe is closed");
		} 
		else {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			ObjectOutputStream objectOutput = new ObjectOutputStream(outputStream);
			
			objectOutput.writeObject(object);
			ByteBuffer buffer = ByteBuffer.wrap(outputStream.toByteArray());
			
			while (buffer.remaining() > 0) {
				socketChannel.write(buffer);
			}
			
			//buffer.clear();
			objectOutput.close();

			
		}
	}
	
	/**
	 * Sets the ClientPhase of this Pipe.
	 * @param phase - the ClientPhase to set this Pipe to
	 */
	public void setPhase(ClientPhase phase){
		this.phase = phase;
	}
	
	/**
	 * Closes this pipe, prohibiting all IO operations on this pipe.
	 * @throws IOException - if an IO error occurs when closing the Pipe
	 */
	public synchronized void closePipe() throws IOException{
		if (closed == false) {
      selectionKey.cancel();
      
		  socketChannel.close();
		  
		  ClientID.removeGeneratedID(clientID.getID());
			closed = true;
		}
	}
	
	/**
	 * Returns the current ClientPhase of this Pipe
	 * @return the current ClientPhase of this Pipe
	 */
	public ClientPhase getPhase(){
		return phase;
	}

	/**
	 * Returns the ClientID of this Pipe
	 * @return the ClientID of this Pipe
	 */
	public ClientID getClientID() {
		return clientID;
	}

	/**
	 * Returns the symmetric AES key used for encrypting
	 * and decrypting parcel contents from this client
	 * @return the symmetric AES key used for encrypting
   * and decrypting parcel contents from this client
	 */
	public SecretKey getAesSecretKey() {
		return aesSecretKey;
	}
	
	/**
	 * Returns the client's remote SocketAddress
	 * @return the client's remote SocketAddress
	 */
	public SocketAddress getRemoteAddress(){
		return remoteAdd;
	}
	
	/**
	 * Returns the String representation of this client
	 * 
	 * The string is formatted as such:
	 * "[ID:123456] [Name:John] | 192.168.0.0"
	 * 
	 * @return the String representation of this client
	 */
	public String toString(){
		return clientID+" | "+remoteAdd;
	}
	
	/**
	 * Returns true if this Pipe is closed 
	 * @return true if closed, false if else
	 */
	public boolean isClosed(){
		return closed;
	}

}
