package jg.skeptik.server.logic;

/**
 * Options that a ConnectionManager supports.
 * 
 * @author Jose Guaro
 */
public interface ConnectionManagerOptions {
  
  /**
   * Stops the acceptance of new clients
   * @param bool - true if client acceptance should be stopped, false if else
   */
  public void stopAccepts(boolean bool);
  
  /**
   * Stops the reading of Parcels, and Parcel exchanges
   * @param bool - true if parcel reading and exchange should be stopped, false if else
   */
  public void stopParcelPassing(boolean bool);
  
  /**
   * Stops the fulfillment of ServerRequests
   * @param bool - true if client acceptance should be stopped, false if else
   */
  public void stopRequests(boolean bool);
  
  /**
   * Returns true if this ConnectionManager 
   * has been set to stop accepting clients, false if else.
   * @return true if this ConnectionManager 
   *         has been set to stop accepting clients, false if else.
   */
  public boolean hasStoppedAccepts();
  
  /**
   * Returns true if this ConnectionManager 
   * has been set to stop parcel exchanges, false if else.
   * @return true if this ConnectionManager 
   *         has been set to stop parcel exchanges, false if else
   */
  public boolean hasStoppedParcelPassing();
  
  /**
   * Returns true if this ConnectionManager has been set to 
   * stop the fulfillment of ServerRequests, false if else.
   * 
   * @return true if this ConnectionManager has been set to stop the 
   *         fulfillment of ServerRequests, false if else.
   */
  public boolean hasStoppedRequests();
}
