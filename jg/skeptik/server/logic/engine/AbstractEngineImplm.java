package jg.skeptik.server.logic.engine;

import java.nio.channels.Selector;

import jg.skeptik.server.logic.PipeDatabase;
import jg.skeptik.server.logic.SkeptikServer;

/**
 * Basic implementation of an AbstractSkeptikEngine
 * @author Jose Guaro
 *
 */
class AbstractEngineImplm extends AbstractSkeptikEngine{

  protected AbstractEngineImplm(SkeptikServer server, Selector selector, PipeDatabase database) {
    super(server, selector, database);
  }

}
