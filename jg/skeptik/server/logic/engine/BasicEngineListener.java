package jg.skeptik.server.logic.engine;

import java.io.Serializable;

import jg.skeptik.common.Address;
import jg.skeptik.server.logic.Pipe;
import jg.skeptik.server.logic.SkeptikServer;

public class BasicEngineListener implements EngineListener {

  @Override
  public void clientPromoted(Pipe client) {
    SkeptikServer.print("CLIENT: [ID:"+client.getClientID().getID()+"] has been promoted to PHASE_2!");
  }

  @Override
  public void clientDisconnected(Pipe client, DisconnectionCause cause) {
    if (cause.dueToException()) {
      SkeptikServer.printError("CLIENT: [ID:"+client.getClientID()+"] disconnected due to: "
          +System.lineSeparator()+cause.getCause());
    }
    else {
      SkeptikServer.print("CLIENT: [ID:"+client.getClientID()+"] disconnected due to: "
          +System.lineSeparator()+cause.getCause());
    }
    
  }

  @Override
  public void parcelRecieved(Serializable received, Address address) {
    SkeptikServer.print("CLIENT: [ID:"+address.getSender()+"] sent a Parcel to [ID:"+address.getRecipient()+"]");
  }

  @Override
  public void engineShutdown() {
    SkeptikServer.print("SKEPTIK SERVER: Shutting down!!");
  }

}
