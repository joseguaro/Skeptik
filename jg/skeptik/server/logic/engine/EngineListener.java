package jg.skeptik.server.logic.engine;

import java.io.Serializable;

import jg.skeptik.common.Address;
import jg.skeptik.common.parcel.Parcel;
import jg.skeptik.server.logic.Pipe;

/**
 * An interface for listening to key engine events, such as a
 * client being promoted to PHASE_2, a client disconnecting, a parcel being received,
 * a request being received, and the server being shutdown.
 * 

 * @author Jose Guaro
 *
 */
public interface EngineListener {
  
  /**
   * Fired when a client has been promoted to PHASE_2
   * 
   * @param client - the client that promoted.
   */
  public void clientPromoted(Pipe client);
  
  /**
   * Fired when a client has disconnected
   * 
   * NOTE: This is triggered after the client's backing socket has been closed
   *       for I/O operations. Any further I/O operations on the client's socket will
   *       throw an error.
   *       
   *       This method is intended mainly for logging and error-diagnosing use.
   *  
   * @param client - the client that disconnected
   */
  public void clientDisconnected(Pipe client, DisconnectionCause cause);
  
  /**
   * Fired when a Parcel is about to be processed by the engine
   * 
   * @param received - the content of the Parcel
   * @param address - the Address that came with the Parcel
   */
  public void parcelRecieved(Serializable received, Address address);
      
  /**
   * Fired when the engine shuts down.
   */
  public void engineShutdown();
  
}
