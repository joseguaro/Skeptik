package jg.skeptik.server.logic.engine;

import jg.skeptik.common.Address;
import jg.skeptik.server.logic.Pipe;

/**
 * Represents an action meant to be executed internally by the Skeptik server
 * 
 * Actions are based on the Code associated with them:
 * - PARCEL - a Parcel meant to be sent to another Client
 * - REQUEST - a ServerRequest meant to be fulfilled
 * - COMMAND - a Command given by an admin, meant to be executed immediately
 * 
 * An Action acts on an object, sent by a Client. Depending on the type of this
 * object - be it a ServerRequest, Command, etc.. - this Action is given a Code
 * that is explained above. 
 * 
 * @author Jose Guaro
 *
 * @param <T> - the type of object this Action acts on
 */
public class Action<T> {

	public enum Code{
		PARCEL, REQUEST, COMMAND, PUBLIC_KEY;
	}
	
	private final Code code;
	private final T pack;
	private final Pipe sender;
	private final Address address;
	
	/**
	 * Constructs an Action
	 * 
	 * @param code - the code detailing the nature of this Action
	 * @param pack - the object this Action acts on
	 * @param sender - the Client whom sent the object this Action acts on
	 * @param parcelAddressTag - the address of the Parcel "pack" was in
	 */
	public Action(Code code, T pack, Pipe sender, Address parcelAddressTag) {
		this.pack = pack;
		this.code = code;
		this.sender = sender;
		this.address = parcelAddressTag;
	}

	/**
	 * Returns the Code of this Action
	 * @return the Code of this Action
	 */
	public Code getCode() {
		return code;
	}

	/**
	 * Returns the object this Action acts on
	 * @return the object this Action acts on
	 */
	public T getPack() {
		return pack;
	}

	/**
	 * Returns the sender - in the form of a Pipe - of the object this Action acts on
	 * @return the sender - in the form of a Pipe - of the object this Action acts on
	 */
	public Pipe getSender() {
		return sender;
	}

	/**
	 * Returns the original address of the Parcel that "pack" came in
	 * @return the original address of the Parcel that "pack" came in
	 */
	public Address getAddress() {
		return address;
	}

}
