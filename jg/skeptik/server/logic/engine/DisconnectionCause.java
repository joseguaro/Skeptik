package jg.skeptik.server.logic.engine;

/**
 * Details the cause of a client's disconnection.
 * 
 * @author Jose Guaro
 *
 */
public class DisconnectionCause {
  
  /**
   * A template string for disconnection reasons that are caused by voluntary
   * exits by clients. 
   * 
   * It is a formatted string that requires the input of the client's numerical ID
   */
  public static final String VOLUNARY = "CLIENT: [ID:%d] disconnected voluntarily";
  
  /**
   * A template string for disconnection due to a server shutdown. 
   * 
   * This is mainly aimed for GOOD_BYE messages sent to clients when a Skpetik server
   * is about to shutdown
   * 
   * It is a formatted string that requires the input of the client's numerical ID
   */
  public static final String SERVER_SHTDOWN = "CLIENT: [ID:%d] disconnected by server due to shutdown";
  
  private boolean dueToException;
  
  private String cause;
  private Exception exception;
  
  /**
   * Constructs a DisconnectionCause, with a String detailing what caused 
   * the disconnection and a boolean that determines whether the disconnection
   * was caused by an error
   * @param cause - a String detailing what caused the disconnection
   * @param dueToException - if the disconnection was caused by an Exception
   */
  public DisconnectionCause(String cause, boolean dueToException) {
    this.cause = cause;
    this.dueToException = dueToException;
  }
  
  /**
   * Constructs a DisconnectionCause, with the Exception that caused the disconnection
   * @param exception - the Exception that caused the disconnection
   */
  public DisconnectionCause(Exception exception) {
    this.exception = exception;
    cause = exception.getMessage();
    dueToException = true;
  }
  
  /**
   * Attaches an Exception
   * @param exception - the Exception to be attached
   */
  public void attachException(Exception exception) {
    this.exception = exception;
  }
  
  /**
   * Returns if the cause of the disconnection was due to an exception
   * @return a boolean that determines whether the cause of the 
   *         disconnection was due to an exception
   */
  public boolean dueToException() {
    return dueToException;
  }
  
  /**
   * Returns the String message that details the cause of the disconnection
   * @return the String message that details the cause of the disconnection
   */
  public String getCause() {
    return cause;
  }
  
  /**
   * Returns the attached Exception
   * @return the attached Exception, or null if none was attached
   */
  public Exception getAttatchedException() {
    return exception;
  }
}
