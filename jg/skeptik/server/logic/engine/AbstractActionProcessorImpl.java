package jg.skeptik.server.logic.engine;

import jg.skeptik.server.logic.ConnectionManager;
import jg.skeptik.server.logic.SkeptikServer;

/**
 * A basic implementation of an AbstractActionProcessor
 * @author Jose Guaro
 *
 */
class AbstractActionProcessorImpl extends AbstractActionProcessor{

  protected AbstractActionProcessorImpl(SkeptikServer server, ConnectionManager manager, AbstractSkeptikEngine engine) {
    super(server, manager, engine);
  }

}
