package jg.skeptik.server.logic.engine;

import java.io.Serializable;

import jg.skeptik.common.admin.Command;
import jg.skeptik.common.admin.Result;
import jg.skeptik.common.request.ServerRequest;
import jg.skeptik.common.request.ServerResponce;

/**
 * An ActionProcessor processes Actions, usually for an Engine
 * 
 * @author Jose Guaro
 */
public interface ActionProcessor {
  
  /**
   * Processes a Parcel
   * @param action - the Action that contains the Parcel's contents and information.
   */
  void processParcel(Action<Serializable> action);
  
  /**
   * Processes a ServerRequest
   * @param action - the Action that contains the ServerRequest and information
   *                 related to it.
   * @return a ServerResponce
   */
  ServerResponce processRequest(Action<ServerRequest> action);
  
  /**
   * Processes a Command
   * @param action - the Action that contains the Command and information
   *                 related to it.
   * @return the Result of fulfilling the Command
   */
  Result processCommand(Action<Command> action);
  
}
