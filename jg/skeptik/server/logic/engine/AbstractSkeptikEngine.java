package jg.skeptik.server.logic.engine;

import java.io.IOException;
import java.io.Serializable;
import java.nio.channels.Selector;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;

import jg.skeptik.common.Address;
import jg.skeptik.common.ClientID;
import jg.skeptik.common.ClientPhase;
import jg.skeptik.common.admin.Command;
import jg.skeptik.common.admin.Result;
import jg.skeptik.common.parcel.Parcel;
import jg.skeptik.common.parcel.ParcelContent;
import jg.skeptik.common.request.ServerRequest;
import jg.skeptik.common.request.ServerResponce;
import jg.skeptik.server.config.BasicConfigVars;
import jg.skeptik.server.logic.ConnectionManager;
import jg.skeptik.server.logic.Pipe;
import jg.skeptik.server.logic.PipeDatabase;
import jg.skeptik.server.logic.SkeptikServer;
import jg.skeptik.server.logic.engine.Action.Code;
import jg.skeptik.server.logic.workers.Task;
import jg.skeptik.server.logic.workers.Task.TaskType;
import jg.skeptik.server.logic.workers.ThreadPool;

/**
 * An implementation of a bare Skeptik engine
 * 
 * @author Jose Guaro
 */
public abstract class AbstractSkeptikEngine implements Engine{
  
  protected final SkeptikServer server;
  protected final PipeDatabase pipeDatabase;
  protected final Selector selector;
  protected final ThreadPool threadPool;
  
  protected final Queue<Action<?>> actions;
  
  protected ConnectionManager connectionManager;
  
  protected boolean initialized;
  protected boolean hasStarted;
  protected boolean stopEngine;
  
  protected List<EngineListener> listeners; 
  
  /**
   * Constructs an AbstractSkeptikEngine
   * @param server - the SkeptikServer this engine is executing Actions for
   * @param selector - the Selector used for IO operations
   * @param database - the PipeDatabase that the server uses
   */
  protected AbstractSkeptikEngine(SkeptikServer server, Selector selector, PipeDatabase database) {
    this.server = server;
    this.pipeDatabase = database;
    this.selector = selector;
    this.actions = new ConcurrentLinkedQueue<>();
    this.threadPool = new ThreadPool((int) server.getConfigs().getValue(BasicConfigVars.THREAD_ALLOTED_AMNT));
    this.listeners = new ArrayList<>();
  }
  
  /**
   * Initializes this AbstractSkeptikEngine with the ConnectionManager that will
   * feed it Actions related to Client activity, and initializes the 
   * thread pool that this engine will be executing.
   * 
   * @param manager - the ConnectionManager that will feed it Actions
   */
  @Override
  public void initialize(ConnectionManager manager) {
    if (!initialized) {
      initialized = true;
      connectionManager = manager;
      
      threadPool.initWorkers();
    }
  }
  
  /**
   * Submits an Action for this Engine to execute
   * @param action - the Action to execute
   */
  @Override
  public void submitAction(Action<?> action) {
    actions.add(action);
  }
  
  /**
   * Starts this Engine with the ActionProcessor
   * that will process given internal Actions
   * 
   * This engine will run it's main cycle loop, and ConnectionManager's 
   * startComms() on two concurrent threads using the thread pool it has
   * initialized.
   * 
   * It is advised that when running a Skeptik server, THREAD_ALLOTED_AMNT
   * should be given a value of 2 or more.
   * 
   * @param actionProcessor - the ActionProcessor that will process submitted Actions
   */
  @Override
  public final void start(ActionProcessor actionProcessor) {
    if (!hasStarted) {
      hasStarted = true;
      
      threadPool.submitTask(new Task<>(new Runnable() {
        public void run() {
          connectionManager.startComms();
        }
      }, TaskType.INTERNAL_PROCESS));
            
      threadPool.submitTask(new Task<>(new Runnable() {
        public void run() {
          cycleThrough(actionProcessor);
        }
      }, TaskType.INTERNAL_PROCESS));
    }
  }
  
  private void cycleThrough(ActionProcessor processor) {
    SkeptikServer.print("SKP SERVER: Engine has started!");
    while (stopEngine == false) {
      if (actions.isEmpty() == false) {
        Action<?> action = actions.poll();
        
        if (action.getCode() == Code.PUBLIC_KEY) {
          Action<PublicKey> publicKey = (Action<PublicKey>) action;
          try {
            promoteTo1B(action.getSender(), publicKey.getPack());
            SkeptikServer.print("SKP SERVER: Client "+publicKey.getSender().getClientID()+" has been promoted to PHASE_2!");
            
            /*
             * Triggers all listeners about the promotion
             */
            for(EngineListener listener:  listeners) {
              listener.clientPromoted(publicKey.getSender());
            }
            
          } catch (Exception e) {
            /*
             * Triggers all listeners about the error
             */
            for(EngineListener listener:  listeners) {
              listener.clientDisconnected(publicKey.getSender(), new DisconnectionCause(e));
            }
            
            e.printStackTrace();
          }
        }
        else {
          if (action.getCode() == Code.PARCEL) {
            Action<Serializable> parcelAction = (Action<Serializable>) action;

            /*
             * Triggers all listeners about the parcel
             */
            for(EngineListener listener:  listeners) {
              listener.parcelRecieved(parcelAction.getPack(), parcelAction.getAddress());
            }
            
            processor.processParcel(parcelAction);
          }
          else if (action.getCode() == Code.REQUEST) {
            System.out.println("SKP SERVER: Client ["+action.getAddress().getSender()+"] sent a request!");
            Action<ServerRequest> requestAction = (Action<ServerRequest>) action;
            ServerResponce responce = processor.processRequest(requestAction);
            
            Address newAddress = requestAction.getAddress().getReverse();
            
            try {
              Cipher cipher = Cipher.getInstance("AES");
              cipher.init(Cipher.ENCRYPT_MODE, requestAction.getSender().getAesSecretKey());
              SealedObject sealedObject = new SealedObject(responce, cipher);
              
              Parcel parcel = new Parcel(newAddress, sealedObject, ParcelContent.SERVER_RESPONCE);
              requestAction.getSender().write(parcel);
            } catch (GeneralSecurityException | IOException e) {

              /*
               * Triggers all listeners about the error
               */
              for(EngineListener listener:  listeners) {
                listener.clientDisconnected(requestAction.getSender(), new DisconnectionCause(e));
              }
              
              try {
                pipeDatabase.removeClient(requestAction.getSender().getClientID());
              } catch (IOException e1) {
                e1.printStackTrace();
              }
            }
          }
          else if (action.getCode() == Code.COMMAND) {
            Action<Command> commandAction = (Action<Command>) action;
            Result result = processor.processCommand(commandAction);

            Address newAddress = commandAction.getAddress().getReverse();

            try {
              Cipher cipher = Cipher.getInstance("AES");
              cipher.init(Cipher.ENCRYPT_MODE, commandAction.getSender().getAesSecretKey());
              SealedObject sealedObject = new SealedObject(result, cipher);

              Parcel parcel = new Parcel(newAddress, sealedObject, ParcelContent.SERVER_RESPONCE);
              commandAction.getSender().write(parcel);
            } catch (GeneralSecurityException | IOException e) {

              /*
               * Triggers all listeners about the error
               */
              for(EngineListener listener:  listeners) {
                listener.clientDisconnected(commandAction.getSender(), new DisconnectionCause(e));
              }
              
              try {
                pipeDatabase.removeClient(commandAction.getSender().getClientID());
              } catch (IOException e1) {
                e1.printStackTrace();
              }
            }
          }
        }
      }
    }
    
    SkeptikServer.print("SKP SERVER: Engine has STOPPED!");
    shutdown();
  }
  
  /**
   * Finishes the key exchange of a client so that they 
   * get promoted to Phase 1B
   * @throws Exception 
   */
  private void promoteTo1B(Pipe client, PublicKey publicKey) throws Exception{
    Cipher cipher = Cipher.getInstance("RSA");
    cipher.init(Cipher.ENCRYPT_MODE, publicKey);
    
    SealedObject encryptedSecKey = new SealedObject(client.getAesSecretKey(), cipher);

    Address address = new Address(server.serverId(), client.getClientID());
    Parcel parcelToSend = new Parcel(address, encryptedSecKey, ParcelContent.MESSAGE);

    client.write(parcelToSend);
    client.setPhase(ClientPhase.PHASE_2);
  }
  
  private SealedObject seal(Serializable object, SecretKey key) {
    try {
      Cipher cipher = Cipher.getInstance("AES");
      cipher.init(Cipher.ENCRYPT_MODE, key);
      return new SealedObject(object, cipher);
    } catch (InvalidKeyException e) {
      e.printStackTrace();
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (NoSuchPaddingException e) {
      e.printStackTrace();
    } catch (IllegalBlockSizeException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }
  
  private void sendGoodbyes() {
    List<ClientID> clientIDs = pipeDatabase.idList();
    
    String goodbye = (String) server.getConfigs().getValue(BasicConfigVars.SERVER_BYE);
    
    for(ClientID id: clientIDs) {
      Pipe pipe = pipeDatabase.getClient(id);
      if (pipe != null) {
        SealedObject sealed = seal(goodbye, pipe.getAesSecretKey());
        Parcel parcel = new Parcel(new Address(pipe.getClientID(), server.serverId()), sealed, ParcelContent.MESSAGE);
        try {
          pipe.write(parcel);
          System.out.println("WROTE TO: "+id.getID());
          pipe.closePipe();
          
          for(EngineListener listener: listeners) {
            String discMess = String.format(DisconnectionCause.SERVER_SHTDOWN, pipe.getClientID().getID());
            listener.clientDisconnected(pipe, 
                  new DisconnectionCause(discMess, false));
          }
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    
    
  }

  private void shutdown() {
    actions.clear();
    connectionManager.stopComms();
    
    //blocks until ConnectionManager has actually stopped
    while (connectionManager.hasStoppedComms() == false);
    
    threadPool.shutdown();
    
    sendGoodbyes();
    
    //removes and disconnects all clients from the database
    List<ClientID> clientIDs = pipeDatabase.idList();
    for(ClientID id: clientIDs) {
      try {
        pipeDatabase.removeClient(id);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    
    try {
      
      for(EngineListener listener: listeners) {
        listener.engineShutdown();
      }
      
      server.shutdown();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Stops this Engine and releases any used resources
   * @param bool - true if engine should shutdown, false if else
   */
  public void stopEngine(boolean bool) {
    stopEngine = bool;
  }
  
  /**
   * Returns the PipeDatabase this Engine is utilizing
   * @return the PipeDatabase this Engine is utilizing
   */
  public PipeDatabase database() {
    return pipeDatabase;
  }
  
  /**
   * Adds an EngineListener to this engine
   * @param listener - the Listener add
   */
  public void addListener(EngineListener listener) {
    listeners.add(listener);
  }
  
  /**
   * Removes an EngineListener to this engine
   * @param listener - the EngineListener to remove
   * @return a boolean that determines whether 'listener' was found among the current
   *         listeners of this Engine
   */
  public boolean removeListener(EngineListener listener) {
    return listeners.remove(listener);
  }
  
  /**
   * Returns the array of EngineListeners listening to this engine
   * @return the array of EngineListeners listening to this engine
   */
  public EngineListener[] getListeners() {
    return listeners.toArray(new EngineListener[listeners.size()]);
  }
  
  /**
   * Returns an instance of a basic implementation of an AbstractSkeptikEngine.
   * @param server - the SkeptikServer this engine is executing Actions for
   * @param selector - the Selector used for IO operations
   * @param database - the PipeDatabase that the server uses
   * @return a basic implementation of an AbstractSkeptikEngine.
   */
  public static AbstractSkeptikEngine open(SkeptikServer server, Selector selector, PipeDatabase database) {
    return new AbstractEngineImplm(server, selector, database);
  }
}
