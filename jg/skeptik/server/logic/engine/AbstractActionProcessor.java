package jg.skeptik.server.logic.engine;

import java.io.IOException;
import java.io.Serializable;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SealedObject;

import jg.skeptik.common.admin.BasicCommandCode;
import jg.skeptik.common.admin.Command;
import jg.skeptik.common.admin.Result;
import jg.skeptik.common.parcel.Parcel;
import jg.skeptik.common.parcel.ParcelContent;
import jg.skeptik.common.request.BasicRequestCodes;
import jg.skeptik.common.request.RequestCodes;
import jg.skeptik.common.request.RequestStatus;
import jg.skeptik.common.request.ServerRequest;
import jg.skeptik.common.request.ServerResponce;
import jg.skeptik.server.config.BasicConfigVars;
import jg.skeptik.server.logic.ConnectionManager;
import jg.skeptik.server.logic.Pipe;
import jg.skeptik.server.logic.PipeDatabase;
import jg.skeptik.server.logic.SkeptikServer;

/**
 * An implementation of a bare ActionProcessor for a Skeptik Engine
 * @author Jose Guaro
 *
 */
public abstract class AbstractActionProcessor implements ActionProcessor{
  
  protected SkeptikServer server;
  protected ConnectionManager manager;
  protected AbstractSkeptikEngine engine;
  protected PipeDatabase database;
  
  /**
   * Constructs an AbstractActionProcessor
   * @param server - the SkeptikServer it acts on
   * @param manager - the ConnectionManager that provides the Actions
   * @param engine - the AbstratSkeptikEngine it processes Actions for
   */
  protected AbstractActionProcessor(SkeptikServer server, ConnectionManager manager, AbstractSkeptikEngine engine ) {
    this.server = server;
    this.manager = manager;
    this.engine = engine;
    this.database = engine.database();
  }
  
  /**
   * Processes a Parcel
   * @param action - the Action that contains the Parcel's contents and information.
   */
  @Override
  public void processParcel(Action<Serializable> action) {
    
    Pipe recipient = database.getClient(action.getAddress().getRecipient());
    if (recipient != null) {
      try {
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, recipient.getAesSecretKey());
        
        SealedObject sealedObject = new SealedObject(action.getPack(), cipher);
        Parcel parcel = new Parcel(action.getAddress(), sealedObject, ParcelContent.MESSAGE);
        recipient.write(parcel);
      } catch (InvalidKeyException e) {
        e.printStackTrace();
      } catch (NoSuchAlgorithmException e) {
        e.printStackTrace();
      } catch (NoSuchPaddingException e) {
        e.printStackTrace();
      } catch (IllegalBlockSizeException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
  
  /**
   * Processes a ServerRequest
   * @param action - the Action that contains the ServerRequest and information
   *                 related to it.
   * @return a ServerResponce
   */
  @Override
  public ServerResponce processRequest(Action<ServerRequest> action) {
    RequestCodes code = action.getPack().getReqCode();
    
    ServerRequest req = action.getPack();

    Pipe pipe = database.getClient(action.getAddress().getSender());
    if (pipe == null) {
      return null;
    }
    else {
      if (code == BasicRequestCodes.RENAME_ME) {
        pipe.getClientID().setName((String) req.getValue());
        return new ServerResponce(req, pipe.getClientID().getName() , RequestStatus.APPROVED);

      }
      else if (code == BasicRequestCodes.ATTENTION) {
        return new ServerResponce(req, 
            server.getConfigs().getValue(BasicConfigVars.GREET), 
            RequestStatus.APPROVED);

      }
      else if (code == BasicRequestCodes.SERVER_ID) {
        return new ServerResponce(req, server.serverId(), RequestStatus.APPROVED);
      }
      else if (code == BasicRequestCodes.CLIENT_AMOUNT) {
        return new ServerResponce(req, database.size(), RequestStatus.APPROVED);
      }
      else if (code == BasicRequestCodes.CLIENT_LIST) {
        return new ServerResponce(req, database.idList(), RequestStatus.APPROVED);
      }
      else if (code == BasicRequestCodes.DISCONNECT) {
        int clientID = (int) req.getValue();
        try {
          database.removeClient(clientID);
        } catch (IOException e) {
          e.printStackTrace();
        }
        return new ServerResponce(req, server.getConfigs().getValue(BasicConfigVars.SERVER_BYE), RequestStatus.APPROVED);
      }
      else {
        return new ServerResponce(req, "INVALID REQUEST", RequestStatus.REJECTED);
      }
    }

  }
  
  /**
   * Processes a Command
   * @param action - the Action that contains the Command and information
   *                 related to it.
   * @return the Result of fulfilling the Command
   */
  @Override
  public Result processCommand(Action<Command> action) {
    Command command = action.getPack();
    if (command.getCode() == BasicCommandCode.BLOCK) {
      manager.blockAddress((String) command.getValue());
      return new Result(command, command.getValue());
    }
    else if (command.getCode() == BasicCommandCode.UNBLOCK) {
      boolean found = manager.blockedAddresses().remove((String) command.getValue());
      return new Result(command, found);
    }
    else if (command.getCode() == BasicCommandCode.KICK) {
      boolean found;
      try {
        found = database.removeClient((int) command.getValue());
        return new Result(command, found);
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      return new Result(command, false);
    }
    else if (command.getCode() == BasicCommandCode.GET_IP) {
      Pipe pipe = database.getClient((int) command.getValue());
      if (pipe == null) {
        return new Result(command, null);
      }
      else {
        return new Result(command, pipe.getRemoteAddress().toString());
      }
    }
    else if (command.getCode() == BasicCommandCode.REGEN_KEYS) {
      String keyPath = (String) server.getConfigs().getValue(BasicConfigVars.ADMIN_KEY_PATH);
      try {
        server.getAdminManager().regenerateKey(keyPath);
        return new Result(command, server.getAdminManager().privateKey());
      } catch (NoSuchAlgorithmException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      }
      return new Result(command, null);
    }
    
    else if (command.getCode() == BasicCommandCode.GET_BLOCK_LIST) {
      return new Result(command, manager.blockedAddresses());
    }
    else if (command.getCode() == BasicCommandCode.SHTDWN_MESS_PASS) {
      boolean bool = (boolean) command.getValue();
      manager.stopParcelPassing(bool);
      return new Result(command, manager.hasStoppedParcelPassing());
    }
    else if (command.getCode() == BasicCommandCode.SHTDWN_ACCEPT) {
      boolean bool = (boolean) command.getValue();
      manager.stopAccepts(bool);
      return new Result(command, manager.hasStoppedAccepts());
    }
    else if (command.getCode() == BasicCommandCode.SHTDWN_REQ_PROC) {
      boolean bool = (boolean) command.getValue();
      manager.stopRequests(bool);
      return new Result(command, manager.hasStoppedRequests());
    }
    return null;

  }

  /**
   * Returns an instance of a basic implementation of an AbstractActionProcessor
   * @param server - the SkeptikServer it acts on
   * @param manager - the ConnectionManager that provides the Actions
   * @param engine - the AbstratSkeptikEngine it processes Actions for
   * @return an instance of a basic implementation of an AbstractActionProcessor
   */
  public static ActionProcessor getDefault(SkeptikServer server, ConnectionManager manager, AbstractSkeptikEngine engine) {
    return new AbstractActionProcessorImpl(server, manager, engine);
  }
  
  
}
