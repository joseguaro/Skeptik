package jg.skeptik.server.logic.engine;

import jg.skeptik.server.logic.ConnectionManager;

/**
 * An engine executes internal actions for a Skeptik server.
 * @author Jose Guaro
 *
 */
public interface Engine {
  
  /**
   * Initializes this Engine with the ConnectionManager that will
   * feed Actions related to Client activity
   * @param manager
   */
  public void initialize(ConnectionManager manager);
  
  /**
   * Submits an Action for this Engine to execute
   * @param action - the Action to execute
   */
  public void submitAction(Action<?> action);
  
  /**
   * Starts this Engine with the ActionProcessor
   * that will process given internal Actions
   * @param actionProcessor
   */
  public void start(ActionProcessor actionProcessor);
  
  /**
   * Stops this Engine and releases any used resources
   * @param bool - true if engine should shutdown, false if else
   */
  public void stopEngine(boolean bool);
  
  /**
   * Adds an EngineListener to this engine
   * @param listener - the Listener add
   */
  public void addListener(EngineListener listener);
  
  /**
   * Removes an EngineListener to this engine
   * @param listener - the EngineListener to remove
   * @return a boolean that determines whether 'listener' was found among the current
   *         listeners of this Engine
   */
  public boolean removeListener(EngineListener listener);
  
  /**
   * Returns the array of EngineListeners listening to this engine
   * @return the array of EngineListeners listening to this engine
   */
  public EngineListener[] getListeners();
}
