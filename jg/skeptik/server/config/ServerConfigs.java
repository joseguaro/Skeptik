package jg.skeptik.server.config;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * A set of configuration values that a Skeptik server follows
 * 
 * @author Jose Guaro
 */
public class ServerConfigs implements Serializable{
	
	/**
   * Current release version is 1L
   */
  private static final long serialVersionUID = 1L;
  private static final HashMap<ConfigVariable, Object> BASIC_CONFIG;
	
	static{
	  BASIC_CONFIG = new HashMap<>();
	  
	  BASIC_CONFIG.put(BasicConfigVars.THREAD_ALLOTED_AMNT, 2);
    BASIC_CONFIG.put(BasicConfigVars.SERVER_BYE, "GOODBYE");
    BASIC_CONFIG.put(BasicConfigVars.AES_KEY_SIZE, 128);
    BASIC_CONFIG.put(BasicConfigVars.SERVER_ID_USERNAME, "_SERVER_");
    BASIC_CONFIG.put(BasicConfigVars.CON_TIME_OUT, Integer.MAX_VALUE);
    BASIC_CONFIG.put(BasicConfigVars.SELECTOR_TIME_OUT, 50);
    BASIC_CONFIG.put(BasicConfigVars.GREET, "hello");
    BASIC_CONFIG.put(BasicConfigVars.ADMIN_KEY_PATH, "keys/");
	}

	private final HashMap<ConfigVariable, Object> values;

	/**
	 * Constructs a ServerConfigs with a Map of ConfigVariables and 
	 * values
	 * @param values - a Map of ConfigVariables and associated values
	 */
	public ServerConfigs(Map<ConfigVariable, Object> values) {
	  this.values = new HashMap<>(values);
	}
	
	/**
	 * Clears the current configuration map and loads the configurations
	 * from the parameter
	 * @param values - the Map to overwrite the current mapping with
	 */
	protected synchronized void loadFrom(Map<ConfigVariable, Object> values){
		values.clear();
		values.putAll(values);
	}
	
	/**
	 * Changes a value in the current map
	 * @param varToChange - the ConfigVariable to change
	 * @param newVal - the new value
	 * @return the old value associated with the ConfigVariable
	 */
	protected synchronized Object changeValue(ConfigVariable varToChange, Object newVal){
		return values.put(varToChange, newVal);
	}

	/**
	 * Returns the associated value of the given ConfigVariable
	 * @param vars - the ConfigVariabel to get
	 * @return the associated value
	 */
	public synchronized Object getValue(ConfigVariable vars){
		return values.get(vars);
	}
	
	/**
	 * Returns the current mapping
	 * @return the current mapping
	 */
	public synchronized Map<ConfigVariable, Object> getValueMap(){
		return new HashMap<>(values);
	}
	
	/**
	 * Returns the basic configurations
	 * @return the basic configurations
	 */
	public static ServerConfigs getBasicConfig(){
		return new ServerConfigs(new HashMap<>(BASIC_CONFIG));
	}
}
