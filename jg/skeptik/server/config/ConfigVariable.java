package jg.skeptik.server.config;

/**
 * Variables that specify a server configuration value
 * 
 * @author Jose Guaro
 *
 */
public interface ConfigVariable {}
