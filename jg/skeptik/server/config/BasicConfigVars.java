package jg.skeptik.server.config;

/**
 * Basic configuration variables that a Skeptik server
 * should have defined
 * @author Jose Guaro
 *
 */
public enum BasicConfigVars implements ConfigVariable{
  
  /**
   * Amount of threads alloted to the server's thread pool
   * It is advised that this value should at least be 2 for
   * a Skeptik server to run properly.
   * An int.
   */
  THREAD_ALLOTED_AMNT,  
    
  /**
   * String sent to a client during diconnection or shutdown
   * A String.
   */
  SERVER_BYE, 
  
  /**
   * The key length to be used in AES secret key creation
   * An int.
   */
  AES_KEY_SIZE,  
  
  /**
   * The String ID name of this server
   * A String.
   */
  SERVER_ID_USERNAME, 
  
  /**
   * The socket timeout for this server when waiting for 
   * a client to connect
   * An int. 
   */
  CON_TIME_OUT,
  
  /**
   * The amount of time (in milliseconds) for this server's selector
   * to block for when waiting for Parcel.
   * An int.
   */
  SELECTOR_TIME_OUT,
  
  /**
   * A String message sent to greet a client - either as a connection check
   * or because it was requested.
   * A String.
   */
  GREET,
  
  /**
   * A String file path to a location/directory to where the private keys
   * for unlocking the AdminConsole will be written at.
   * A String.
   */
  ADMIN_KEY_PATH;
}
