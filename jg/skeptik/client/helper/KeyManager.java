package jg.skeptik.client.helper;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

import javax.crypto.SecretKey;

/**
 * A class that generates and stores an RSA public/private key of a client
 * used during the handshake phase of a Skeptik connection
 * and the AES secret key used to encrypt and decrypt data coming
 * from the Skeptik Server and to this client, and vice-verse
 * @author Jose Guaro
 *
 */
public class KeyManager {
	
	private volatile KeyPair rsaPair;
  private volatile SecretKey aeSecretKey;

  /**
   * Constructs a KeyManager. At construction, an RSA keypair with
   * the given keysize is generated. 
   * 
   * @throws NoSuchAlgorithmException - if RSA is not supported by the running system
   */
	public KeyManager(int keysize) throws NoSuchAlgorithmException {
		KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
		generator.initialize(keysize);
		rsaPair = generator.generateKeyPair();
	}
	
	/**
	 * Generates a new RSA keypair
	 * 
	 * @param keysize - size of RSA keypair
	 * @throws NoSuchAlgorithmException - if RSA is not supported by the running system
	 */
	public void regenerateKeyPair(int keysize) throws NoSuchAlgorithmException{
		KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
		generator.initialize(keysize);
		rsaPair = generator.generateKeyPair();
	}
	
	/**
	 * Sets the AES key to be stored in this KeManager
	 * @param secretKey
	 */
	public void setAESKey(SecretKey secretKey){
		aeSecretKey = secretKey;
	}
	
	/**
	 * Returns the stored AES key, or null if no such key was
	 * stored
	 * @return the AES key stored in this KeyManager, or null
	 *         if there was no AES key stored
	 */
	public SecretKey getAESKey(){
		return aeSecretKey;
	}
	
	/**
	 * Returns the RSA keypair of stored in this 
	 * KeyManager
	 * @return the RSA keypair
	 */
	public KeyPair getRSAPair(){
		return rsaPair;
	}

}
