package jg.skeptik.client.logic;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * An IO tunnel is where objects from a SocketChaneel can be received or 
 * sent without the need of allocating resources manually.
 *
 * An IOTunnel can only be closed once, and once closed, all IO operations on this
 * tunnel may result in an error.
 * 
 * @author Jose Guaro
 *
 */
public class IOTunnel {
	
	private final SocketChannel channel;
	
	private volatile boolean closed;
	
	/**
	 * Constructs an IO Tunnel, with a given SocketChannel
	 * 
	 * @param channel - the SocketChannel backing this IOTunnel
	 * @throws IllegalArgumentExceptio - if channel is null
	 */
	public IOTunnel(SocketChannel channel) throws IllegalArgumentException{
		if (channel == null) {
			throw new IllegalArgumentException("the provided SocketChannel is null!");
		}
		else {
			this.channel = channel;
		}
	}

	/**
	 * Send/Writes an object to the underlying SocketChannel of this IOTunnel
	 * 
	 * @param object - the Object to send
	 * @throws IOException - when an I/O error occurs
	 */
	public synchronized void send(Object object) throws IOException{
		if (closed) {
			throw new IllegalStateException("IOTunnel is closed!");
		}
		else {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			ObjectOutputStream objectOutput = new ObjectOutputStream(outputStream);
			
			objectOutput.writeObject(object);
			ByteBuffer buffer = ByteBuffer.wrap(outputStream.toByteArray());
			
			while (buffer.remaining() > 0) {
				channel.write(buffer);
			}
			
			//buffer.clear();
			objectOutput.close();
		}
	}
	
	/**
	 * Attempts to read an Object from this tunnel
	 * 
	 * @return the read Object, or null if there is no Object present
	 * @throws IOException - if an I/O error occurs
	 * @throws ClassNotFoundException - if the read Object is of an unsupported type
	 */
	public synchronized Object read() throws IOException, ClassNotFoundException{
		if (closed) {
			throw new IllegalStateException("IOTunnel is closed!");
		}
		else {
			ByteBuffer readBuffer = ByteBuffer.allocate(1);
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			
			while (channel.read(readBuffer) > 0) {
				if(readBuffer.hasArray()){
					outputStream.write(readBuffer.array());
					readBuffer.clear();
				}
			}
			
			readBuffer.clear();
			if(outputStream.size() > 0){
				ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
				ObjectInputStream objectInput = new ObjectInputStream(inputStream);
				return objectInput.readObject();
			}
			return null;
		}
	}
	
	/**
	 * Closes this IO tunnel.
	 * All future IO operations on this tunnel will result in an exception.
	 * A closed tunnel cannot be reopened.
	 */
	public void close(){
		closed = true;
	}
	
	/**
	 * Checks if this tunnel is closed.
	 * @return true if the tunnel has been closed, false if else.
	 */
	public boolean isClosed(){
		return closed;
	}
}
