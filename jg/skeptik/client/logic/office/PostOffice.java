package jg.skeptik.client.logic.office;

import java.io.IOException;
import java.io.Serializable;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;

import javax.crypto.Cipher;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;

import jg.skeptik.client.helper.KeyManager;
import jg.skeptik.client.logic.Connector;
import jg.skeptik.client.logic.IOTunnel;
import jg.skeptik.common.Address;
import jg.skeptik.common.ClientPhase;
import jg.skeptik.common.ConnectionInfo;
import jg.skeptik.common.parcel.Parcel;
import jg.skeptik.common.parcel.ParcelContent;

/**
 * Manages the connection with a Skeptik server
 * 
 * A PostOffice has two essential tasks:
 * -Complete the Skeptik handshake when connecting with a Skeptik server
 * -Receive and send Parcels from a Skeptik server
 * 
 * Before a client can receive or send Parcels to Skeptik server, it must
 * first complete the Skeptik handshake, which can be done by calling
 * startSkeptikHandshake().
 * 
 * Once the handshake has finished, Parcel transmission can begin. 
 * 
 * @author Jose Guaro
 */
public class PostOffice {
	private final Selector selector;
	private final Connector connector;
	private final KeyManager keyManager;
	private final IOTunnel tunnel;
	
	private volatile ClientPhase phase;
	private volatile ConnectionInfo connectionInfo;
	
	private volatile boolean endTransmission;
	private volatile boolean hanshakeFinished;
	private volatile boolean startedTransmission;
	
	/**
	 * Constructs a PostOffice with the associated Connector, Selector and IOTunnel.
	 * 
	 * @param connector - the Connector that represents the client
	 * @param selector - the Selector to indicate if an Object is ready to be read
	 *                   from the underlying IO stream
	 * @param tunnel - the IOTunnel on which to read and write Parcels. 
	 * @throws NoSuchAlgorithmException
	 */
	public PostOffice(Connector connector , Selector selector, IOTunnel tunnel) throws NoSuchAlgorithmException {
		this.connector = connector;
		this.selector = selector;
		this.tunnel = tunnel;
		phase = ClientPhase.PHASE_1;
		keyManager = new KeyManager(2048);
		
		endTransmission = false;
	}
	
	/**
	 * Exchanges the required keys with the server 
	 * until this client has reached PHASE 1B.
	 * 
	 * @throws IllegalStateException - when this PostOffice has is already in Phase 1B or above. 
	 */
	public final void startSkeptikHandshake() throws Exception{
		if (phase == ClientPhase.PHASE_1) {
			while (phase == ClientPhase.PHASE_1) {
				selector.select();
				Iterator<SelectionKey> keys = selector.selectedKeys().iterator();

				while (keys.hasNext()) {
					SelectionKey selectionKey = keys.next();
					keys.remove();

					if (selectionKey.isReadable()) {
						Object object = tunnel.read();
						if (connectionInfo == null) {
							connectionInfo = (ConnectionInfo) object;

							/*
							 * RSA key exchange beginning here
							 */
							Address address = new Address(connectionInfo.SERVER_ID, connectionInfo.CURRENT_CLIENT_ID);
							Parcel parcel = new Parcel(address, keyManager.getRSAPair().getPublic(), false, ParcelContent.PUBLIC_KEY);
							tunnel.send(parcel);
							
						}
						else if (keyManager.getAESKey() == null) {
							Parcel parcel = (Parcel) object;
							SealedObject sealedKey = (SealedObject) parcel.getObject();
							
							SecretKey secretKey = (SecretKey) sealedKey.getObject(keyManager.getRSAPair().getPrivate());
							keyManager.setAESKey(secretKey);
							
							phase = ClientPhase.PHASE_2;
							hanshakeFinished = true;
						}
					}
				}
			}
		}
		else {
			throw new IllegalStateException("Handshake already finished");
		}
	}
	
	/**
	 * Starts communication with the Skeptik server. Received Parcels from the 
	 * Skeptik server are passed to the given MessageProcessor.
	 * 
	 * @param processor - the MessageProcessor that will process Parcels received from
	 *                    a Skeptik server. 
	 * 
	 * @throws IOException
	 */
	public void startTransmission(MessageProcessor processor) throws IOException{
		startedTransmission = true;

		while (endTransmission == false) {
			selector.select();
			Iterator<SelectionKey> keys = selector.selectedKeys().iterator();

			while (keys.hasNext() && endTransmission == false) {
				SelectionKey selectionKey = keys.next();
				keys.remove();

				if (selectionKey.isReadable()  && endTransmission == false) {
					try {
						Parcel parcel = (Parcel) tunnel.read();
						try {
              Object content = decrypt((SealedObject) parcel.getObject());
              if (parcel.getAddress().getSender() == connectionInfo.SERVER_ID.getID()) {
                System.out.println("----CLIENT: "+connectionInfo.CURRENT_CLIENT_ID+" -- GOT A MESS FROM SERVER");
                System.out.println("              --------CONTENT: "+content.toString());
              }
              processor.processParcel(content, parcel.getContentType(), parcel.getAddress());
              //System.out.println(" OF CLIENT: "+connectionInfo.CURRENT_CLIENT_ID.getID()+" -- END_TRANSMIS? "+endTransmission);
            } catch (InvalidKeyException e) {
              e.printStackTrace();
            }
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		/*
		 * Should I send the goodbye message to the Skeptik server at this point?
		 */
		try {
		  Connector.printToStdOut("*CLIENT: PostOffice has stopped! ");
			shutDownSequence();
			Connector.printToStdOut("*CLIENT"+connectionInfo.CURRENT_CLIENT_ID+": Shutdown hook completed!!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	/**
	 * Starts the shut down sequence of this Post office.
	 * Once this method is called, the IO Tunnel is shutdown, all Jobs are cleared,
	 * and the Client's socket is closed. 
	 * 
	 * @throws Exception 
	 */
	private void shutDownSequence() throws Exception{		
	  connector.disconnect();
	}
	
	private SealedObject encrypt(Serializable object, SecretKey key) throws Exception{
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		
		SealedObject sealedObject = new SealedObject(object, cipher);
		return sealedObject;
	}
	
	/**
	 * Decrypts a SealedObject using this PostOffice's key
	 * @param sealedObject - the SealedObject to decrypt
	 * @return the unsealed object
	 * @throws IOException 
	 * @throws NoSuchAlgorithmException 
	 * @throws ClassNotFoundException 
	 * @throws InvalidKeyException 
	 * @throws Exception 
	 */
	public Object decrypt(SealedObject sealedObject) throws InvalidKeyException, IOException{		
		try {
      return sealedObject.getObject(keyManager.getAESKey());
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
		return null;
	}
	
	/**
	 * Sends a Serializable object to the Skeptik server that is meant to be given
	 * to the Client with the given Client ID
	 * 
	 * Such object is encrypted prior to sending. 
	 * 
	 * @param message - the Serializable object to send
	 * @param clientID - the client to send the object to
	 * @param type - the category of "message" as a ParcelContent
	 * @throws Exception
	 */
	public final synchronized void sendObject(Serializable message, int clientID,
			                                      ParcelContent type) throws Exception{
		Address address = new Address(clientID, connectionInfo.CURRENT_CLIENT_ID.getID());
		
		SealedObject sealedObject = encrypt(message, keyManager.getAESKey());
		Parcel parcel = new Parcel(address, sealedObject, type);
		
		tunnel.send(parcel);
	}
	
	/**
	 * Ends transmission with the Skeptik server
	 * @param bool
	 */
	public void endTransmission(boolean bool){
		endTransmission = bool;
	}
	
	/**
	 * Returns a boolean indicating whether this PostOffice has started
	 * communicating with a Skeptik Server
	 * @return a boolean indicating whether this PostOffice has started
	 * communicating with a Skeptik Server
	 */
	public boolean hasStartedTransmission(){
		return startedTransmission;
	}
	
	/**
	 * Returns a boolean indicating whether this PostOffice has finished
	 * the Skeptik handshake with a Skeptik server. 
	 * @return a boolean indicating whether this PostOffice has finished
	 * the Skeptik handshake with a Skeptik server. 
	 */
	public boolean hasFinishedHandshake(){
		return hanshakeFinished;
	}
	
	/**
	 * Returns the ConnectionInfo or null if the Skeptik handshake has not been completed. 
	 * @return the ConnectionInfo or null if the Skeptik handshake has not been completed.
	 */
	public ConnectionInfo getConnectionInfo(){
		return connectionInfo;
	}

	/**
	 * Returns the current connection phase of this PostOffice
	 * @return the current connection phase of this PostOffice
	 */
	public ClientPhase getPhase(){
		return phase;
	}
}
