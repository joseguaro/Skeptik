package jg.skeptik.client.logic.office;

import jg.skeptik.common.Address;
import jg.skeptik.common.parcel.ParcelContent;

/**
 * Processes incoming Parcels from the server
 * @author Jose Guaro
 *
 */
public interface MessageProcessor {
  
  /**
   * Processes a Parcel.
   * @param content - the content of the Parcel
   * @param contentType - a brief descriptor of the content's type
   */
  public void processParcel(Object content, ParcelContent contentType, Address address);
  
}
