package jg.skeptik.client.logic.office;

import jg.skeptik.client.logic.Connector;

/**
 * A MessageProcessor process messages received from a
 * Skeptik server. 
 * 
 * It has only one abstract method: receiveParcel(). 
 * 
 * receiveParcel() receives a single argument that is a Parcel. This Parcel
 * can contain any Object, and it is up to the implementation code 
 * to decrypt the Parcel and retrieved the contained Object - if the Parcel was 
 * encrypted - and act on the given Object. 
 * 
 * receiveParcel() is called when the PostOffice it is associated with receives 
 * a Parcel from the SkeptikServer. It SHOULD NOT be called by any other
 * component of the code. 
 * 
 * @author Jose Guaro
 *
 */
public abstract class AbstractMessageProcessor implements MessageProcessor{
	
	protected PostOffice postOffice;
	protected Connector connector;
	
	public AbstractMessageProcessor(PostOffice postOffice, Connector connector){
		this.postOffice = postOffice;
		this.connector = connector;
	}
	
	/**
	 * Returns the PostOffice that is associated with this MessageProcessor
	 * @return the PostOffice that is associated with this MessageProcessor
	 */
	public PostOffice getPostOffice(){
		return postOffice;
	}
	
	/**
	 * Returns the Connector that is associated with this MessageProcessor
	 * @return the Connector that is associated with this MessageProcessor
	 */
	public Connector getConnector(){
		return connector;
	}
	
}
