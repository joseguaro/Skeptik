package jg.skeptik.client.logic;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.security.NoSuchAlgorithmException;

import jg.skeptik.client.logic.office.PostOffice;

/**
 * Establishes a connection with a Skeptik server
 * @author Jose Guaro
 *
 */
public class Connector {
	
	private final SocketChannel socket;
	private final Selector selector;
	private final SocketAddress currentAddress;
	private final IOTunnel tunnel;
	
	private volatile PostOffice postOffice;
	
	private volatile boolean isConnected;
	
	/**
	 * Constructs a Connector.
	 * @throws IOException 
	 */
	public Connector() throws IOException {
		socket = SocketChannel.open();
		selector = Selector.open();
		
		socket.configureBlocking(false);
		socket.socket().setSoTimeout(Integer.MAX_VALUE);
		socket.register(selector, SelectionKey.OP_READ);
		isConnected = false;
		currentAddress = socket.getLocalAddress();
		tunnel = new IOTunnel(socket);
	}
	
	/**
	 * Connects to the Skeptik server at the given address and port
	 * @param socketAddress - the address of the Skeptik server
	 * @param port - the port at which the Skeptik server is bound to
	 * @throws IOException - when an IO error occurs
	 */
	public void connect(String socketAddress, int port) throws IOException{
		connect(new InetSocketAddress(socketAddress, port));
	}
	
	/**
	 * Connects to the Skeptik server at the given address and port
	 * @param address - the address of the Skeptik server
	 * @throws IOException - when an IO error occurs
	 */
	public void connect(SocketAddress address) throws IOException{
		if (isConnected) {
			throw new IllegalStateException("Client is already connected");
		}
		else{
			socket.connect(address);
			
			while (socket.finishConnect() == false);
			
			isConnected = true;
		}
	}

	/**
	 * Starts the IO process of this Connector and
	 * returns a PostOffice object to manage message transmissions
	 * to the Skeptik server. 
	 * 
	 * @throws NoSuchAlgorithmException 
	 */
	public PostOffice start() throws NoSuchAlgorithmException{
		if (isConnected) {
			postOffice = new PostOffice(this, selector, tunnel);
			return postOffice;
		}
		throw new IllegalStateException("Not connected to a SkeptikServer");
	}
	
	/**
	 * Disconnects from the Skeptik server.
	 * 
	 * The underlying socket is closed. 
	 * 
	 *  All IO operations made
	 * during/after an invocation of this method may result in an Exception.
	 * @throws IOException 
	 */
	public void disconnect() throws IOException{
		isConnected = false;
		
    selector.close();
    tunnel.close();
    socket.close();
	}
	
	/**
	 * Prints the given String to the standard output of this client
	 * @param string - the String to print
	 */
	public static void printToStdOut(String string){
		System.out.println(string);
	}
	
	/**
	 * Prints the given String to the standard error of this client
	 * @param string - the String to print
	 */
	public static void printToStdErr(String string){
		System.err.println(string);
	}
	
	/** 
	 * Returns the SocketAddress in which this client is bound to
	 * @return the SocketAddress of this Client
	 */
	public SocketAddress getLocalAddress(){
		return currentAddress;
	}
}
