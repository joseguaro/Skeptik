package jg.skeptik.common.request;

/**
 * Represents the status of a submitted ServerRequest
 * @author Jose Guaro
 */
public enum RequestStatus {
  
  /**
   * ServerRequest has been rejected
   */
  REJECTED , 
  
  /**
   * ServerRequest has been received, but no yet fulfilled
   */
  AWAITING, 
  
  /**
   * ServerRequest has been received and fulfilled
   */
  APPROVED;
}
