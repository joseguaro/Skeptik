package jg.skeptik.common.request;

/**
 * Server request codes supported by the current server
 * @author Jose Guaro
 *
 */
public interface RequestCodes {}
