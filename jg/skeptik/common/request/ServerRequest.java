package jg.skeptik.common.request;

import java.io.Serializable;

/**
 * A request to be fulfilled by the server. 
 * 
 * Note: there is no guarantee that a server will fulfill a request.
 * 
 * @author Jose Guaro
 *
 */
public class ServerRequest implements Serializable{

  /**
   * Release version is 1L
   */
  private static final long serialVersionUID = 1L;
  
  private RequestCodes requestCode;
  private Object value;
  
  /**
   * Constructs a ServerRequest with a RequestCode and an Object
   * that presents a value.
   * @param requestCode - the RequestCode of that describes this request
   * @param value - an Object that is necessary to fulfill this request
   */
  public ServerRequest(RequestCodes requestCode, Object value) {
    this.requestCode = requestCode;
    this.value = value;
  }
  
  /**
   * Returns the request code
   * @return the request code
   */
  public RequestCodes getReqCode() {
    return requestCode;
  }
  
  /**
   * Returns the Object associated with this request
   * @return the Object associated with this request
   */
  public Object getValue() {
    return value;
  }

}
