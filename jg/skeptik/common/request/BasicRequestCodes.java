package jg.skeptik.common.request;

/**
 * Request code
 * @author Jose Guaro
 *
 */
public enum BasicRequestCodes implements RequestCodes{
    
  /**
   * Renames the Client who sent this request
   * with the String name they provided.
   */
  RENAME_ME,  
  
  /**
   * Sends the requesting Client a String message/greeting. 
   * This greeting should be set in the ServerConfigs
   */
  ATTENTION,
  
  /**
   * Sends a List of all clients connected to this server.
   * The list should contain ClientIDs
   */
  CLIENT_LIST, 
  
  /**
   * Sends the number of clients conencted to this server.
   * The type used should be an int
   */
  CLIENT_AMOUNT, 
    
  /**
   * Sends the Server's ClientID.
   */
  SERVER_ID, 
  
  /**
   * Disconnects the sending client from this server. 
   */
  DISCONNECT,
}
