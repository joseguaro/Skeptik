package jg.skeptik.common.request;

import java.io.Serializable;

/**
 * Represents a responce to a ServerRequest.
 * @author Jose Guaro
 *
 */
public class ServerResponce implements Serializable{

  /**
   * Release version is 1L
   */
  private static final long serialVersionUID = 1L;
  
  private ServerRequest cause;
  private Object result;
  private RequestStatus status;
  
  /**
   * Constructs a ServerResponce, along with the ServerRequest
   * it is responding to and the result of fulfilling this request
   * @param cause
   * @param result
   * @param status
   */
  public ServerResponce(ServerRequest cause, Object result, RequestStatus status) {
    this.cause = cause;
    this.result = result;
    this.status = status;
  }
  
  /**
   * Returns the original request
   * @return the original request
   */
  public ServerRequest getRequest() {
    return cause;
  }
  
  /**
   * Returns the result of fulfilling the request
   * @return the result of fulfilling the request
   */
  public Object getResult() {
    return result;
  }

  /**
   * Returns the status of the ServerRequest 
   * @return the status of the ServerRequest 
   */
  public RequestStatus getStatus() {
    return status;
  }
}
