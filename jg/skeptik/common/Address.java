package jg.skeptik.common;

import java.io.Serializable;

/**
 * A "sticker" object stamped on to a Parcel that says
 * the sender of the Parcel and its intended recipient
 * @author Jose Guaro
 *
 */
public class Address implements Serializable{
	
	/**
	 * Current release version is 1L
	 */
	private static final long serialVersionUID = 1L;
	
	private final int to;
	private final int from;
	
	/**
	 * Constructs an Address .
	 * @param to - the sending Client's ID object
	 * @param from - the receiving Client's ID object
	 */
	public Address(ClientID to, ClientID from){
		this.to = to.getID();
		this.from = from.getID();
	}
	
	/**
	 * Constructs an Address .
	 * @param to - the sending Client's ID number
	 * @param from - the receiving Client's ID number
	 */
	public Address(int to, int from){
		this.to = to;
		this.from = from;
	}
	
	/**
	 * Returns the receiving Client's ID number
	 * @return the receiving Client's ID number
	 */
	public int getRecipient(){
		return to;
	}
	
	/**
	 * Returns the sending Client's ID number
	 * @return the sending Client's ID number
	 */
	public int getSender(){
		return from;
	}
	
	/**
	 * Creates an Address object where the receiving ID 
	 * and sender ID of this Address are switched
	 * @return an Address whose receiving and sender ID 
	 *         are switched
	 */
	public Address getReverse(){
		return new Address(from, to);
	}
	
	public Object clone() {
		return new Address(to, from);
	}
	
	public String toString(){
		return "To: "+to+"||from: "+from;
	}
}
