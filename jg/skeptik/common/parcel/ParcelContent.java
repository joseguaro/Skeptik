package jg.skeptik.common.parcel;

/**
 * A set of basic content descriptors
 * @author Jose Guaro
 */
public enum ParcelContent{
	/**
	 * Describes a response to server request
	 */
	SERVER_RESPONCE, 
	
	/**
	 * Describes a request to the server
	 */
	SERVER_REQUEST, 
	
	/**
	 * Describes a command to be fulfilled by the server.
	 * This should be sent by some sort of server administrator
	 */
	COMMAND, 

	/**
	 * Describes the result of command being fulfilled
	 * This should be sent by the server to a server administrator
	 */
	RESULT, 
	
	/**
	 * Describes a message/object to be sent to another client
	 */
	MESSAGE,
	
  /**
   * Describes that the content is a public key.
   * This should be used by the Client when sending their
   * PUBLIC_KEY during the Skeptik handshake
   */
	PUBLIC_KEY;
}

