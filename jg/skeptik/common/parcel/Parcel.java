package jg.skeptik.common.parcel;

import java.io.Serializable;

import javax.crypto.SealedObject;

import jg.skeptik.common.Address;

/**
 * Parcels are containers for Objects to be passed between a Client and a Server
 * 
 * All Objects that are meant to be sent across the Skeptik network, that are not meant
 * for Phase 1 communications,  must be contained in a Parcel.
 * 
 * Parcel are meant to be sent over a some byte channel, so the Object it contains must naturally
 * be Serializable.
 * 
 * A Parcel's contents may or may not be encrypted. 
 * 
 * @author Jose Guaro
 *
 */
public final class Parcel implements Serializable{
	/**
   * Current release version is 1L
   */
  private static final long serialVersionUID = 1L;

  private final Address address;
	
	private final Serializable object;  
	private final boolean isEncrytped;
	private final ParcelContent contentType;
	
	
	
	/**
	 * Constructs a Parcel. 
	 * 
	 * @param address - The Address of this Parcel
	 * @param object - the object this Parcel holds
	 * @param isEncrypted - true if object is encrypted, false if else
	 * @param type - the type of Object this parcel is holding
	 */
	public Parcel(Address address, Serializable object, boolean isEncrypted, ParcelContent type){
		this.isEncrytped = isEncrypted;
		this.address = address;
		this.object = object;
		this.contentType = type;
	}
	
	/**
	 * Constructs a Parcel whose content is encrypted
	 * 
	 * @param address - the Address of this Parcel
	 * @param object - the SealedObject this Parcel holds
	 * @param type - the type of Object this parcel is holding
	 */
	public Parcel(Address address, SealedObject object, ParcelContent type){
		this(address, object, true, type);
	}
	
	/**
	 * Checks if the content of this Parcel is encrypted
	 * @return true if it encrypted. False, if else
	 */
	public boolean isEncrypted(){
		return isEncrytped;
	}
	
	/**
	 * Returns the Address of this Parcel
	 * @return Address of this Parcel
	 */
	public Address getAddress(){
		return address;
	}
	
	/**
	 * Returns the contents of this Parcel
	 * @return the contents of this Parcel
	 */
	public Serializable getObject(){
		return object;
	}
	
	/**
	 * Returns the content type of this Parcel's content
	 * @return the content type of this Parcel's content
	 */
	public ParcelContent getContentType(){
		return contentType;
	}
	
}
