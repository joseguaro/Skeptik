package jg.skeptik.common;

/**
 * Describes the current status of a cleint's connection with
 * the Skeptik server
 * @author Jose Guaro
 *
 */
public enum ClientPhase {
  
  /**
   * The client has received a ConnectionInfo object and 
   * the server is waiting for the client to send
   * their public key
   */
  PHASE_1, 
  
  /**
   * The client has sent their public key and the server
   * has sent an encrypted asymmetric key for secure communication
   */
  PHASE_2;
}

