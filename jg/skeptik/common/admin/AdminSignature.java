package jg.skeptik.common.admin;

import java.io.IOException;
import java.io.Serializable;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.SignedObject;

/**
 * Used to legitimize a Command. A Skeptik Server should
 * never execute a Command that has not been signed with
 * an AdminSignature
 * 
 * @author Jose Guaro
 *
 */
public class AdminSignature implements Serializable{
  
  /**
   * Current release version is 1L
   */
  private static final long serialVersionUID = 1L;
  private final PrivateKey privateKey;
  
  /**
   * Constructs an AdminSignature with a PrivateKey
   * to be used for signing commands
   * @param key - the PrivateKey used for signing Commands
   */
  public AdminSignature(PrivateKey key) {
    this.privateKey = key;
  }
  
  /**
   * Signs the given Commands using the private key
   * @param command - the Command to sign 
   * @return a SignedObject that holds the Command.
   */
  public SignedObject sign(Command command) {
    try {
      Signature signature = Signature.getInstance(privateKey.getAlgorithm());
      return new SignedObject(command, privateKey, signature);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (InvalidKeyException e) {
      e.printStackTrace();
    } catch (SignatureException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }
  
}
