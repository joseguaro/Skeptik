package jg.skeptik.common.admin;

import java.io.Serializable;

/**
 * A command is an action meant to be executed by a Skeptik server
 * 
 * Commands are meant to be sent by a server administrator, and are
 * meant to cause administrative server changes.
 * 
 * Unlike requests, commands are guaranteed to be executed or at least
 * return a result - except in the case of a shutdown command.
 * 
 * @author Jose Guaro
 *
 */
public class Command implements Serializable{
  
  /**
   * Release version is 1L
   */
  private static final long serialVersionUID = 1L;
  private final CommandCodes code;
  private final Object value;
  
  public Command(CommandCodes code, Object value){
    this.code = code;
    this.value = value;
  }
  
  public void sign() {
    
  }

  public CommandCodes getCode() {
    return code;
  }

  public Object getValue() {
    return value;
  }


}
