package jg.skeptik.common.admin;

import java.io.Serializable;

/**
 * Represents the result of executing a Command.
 * @author Jose Guaro
 *
 */
public class Result implements Serializable{
  /**
   * Release version is 1L
   */
  private static final long serialVersionUID = 1L;
  private final Command command;
  private final Object result;

  /**
   * Constructs a Result
   * 
   * @param command - the Command this Result is associated with
   * @param result - the actual Result of fulfilling the Command
   */
  public Result(Command command, Object result) {
    this.command = command;
    this.result = result;
  }
  
  /**
   * Returns the associated Command
   * @return the associated Command
   */
  public Command getCommand(){
    return command;
  }
  
  /**
   * Returns the result of fulfilling the associated Command
   * @return the result of fulfilling the associated Command
   */
  public Object getResult(){
    return result;
  }

}
