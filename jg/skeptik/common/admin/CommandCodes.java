package jg.skeptik.common.admin;

/**
 * Set of command codes that describes the command a server executes
 * @author Jose Guaro
 *
 */
public interface CommandCodes {}
