package jg.skeptik.common.admin;

/**
 * Basic command codes that an AdminConsole shoudl support
 * @author Jose Guaro
 *
 */
public enum BasicCommandCode implements CommandCodes{
  
  /**
   * Blocks a certain IP address from connecting to the server.
   * Value should be a String representation of the IP address.
   * -Expected Result - the String representation of the IP address.
   */
  BLOCK ,

  /**
   * Removes the given IP address from the server's block list.
   * Value should be a String representation of the IP address to unblock
   * -Expected Result - a boolean of whether the IP address was in the block list
   */
  UNBLOCK ,

  /**
   * Kicks a Client from the server
   * Value should be the numerical ID of the client
   * -Expected Result - a boolean of whether the client was found
   */
  KICK,   

  /**
   * Returns the String representation of the IP address of a Client
   * Value should be the numerical ID of the client
   * -Expected Result - String representation of the IP address of a Client
   */
  GET_IP, 

  /**
   * Returns a Set of String IP Address that are blocked from connecting with this server
   * Value should be null
   * -Expected Result - a Set of String IP Address
   */
  GET_BLOCK_LIST, 
  
  /**
   * Shuts down the parcel exchange of the server
   * Value should be a boolean - true for shutting down, false for not.
   * -Expected Result - A boolean of whether the process has shutdown
   */
  SHTDWN_MESS_PASS,
  
  /**
   * Shuts down the processing of ServerRequests.
   * Value should be a boolean - true for shutting down, false for not.
   * -Expected Result - A boolean of whether the process has shutdown
   */
  SHTDWN_REQ_PROC, 

  /**
   * Shuts down the acceptance of new clients
   * Value should be a boolean - true for shutting down, false for not.
   * -Expected Result - A boolean of whether the process has shutdown
   */
  SHTDWN_ACCEPT, 

  /**
   * Shuts down the server
   * Value should be a boolean - true for shutting down, false for not.
   * -Expected Result: Do not expect a Result.
   */
  SHTDWN_SERVER, 

  /**
   * Regenerates the keys of AdminConsoles
   * Value should be null
   * -Expected Result: the new Secret keys generated
   */
  REGEN_KEYS, 

}
