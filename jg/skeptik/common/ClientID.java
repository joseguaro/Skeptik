package jg.skeptik.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.TreeSet;

/**
 * An identifier for a client. 
 * 
 * In a Skeptik connection, a Client is identified using
 * a unique numerical ID by both the server and connected
 * clients.
 * 
 * Whenever a Client connects, a new ID is generated for that client.
 * 
 * No two clients have the same numerical ID. However, clients can
 * have the same String name - which can be changed.
 * 
 * The server does have its own ClientID to maintain comparability with 
 * this standard, but it's numerical ID is 0.
 * 
 * @author Jose Guaro
 *
 */
public final class ClientID implements Serializable{
	/**
	 * Release version is 1L
	 */
	private static final long serialVersionUID = 1L;

	private transient static final TreeSet<Integer> idNumsGiven = new TreeSet<>();
	
	private static ClientID SERVER_ID;

	private final int id;
	private volatile String name;

	/**
	 * Constructs a ClientID with a unique numerical id and
	 * a given String id.
	 * @param name - the given String id
	 */
	public ClientID(String name){
		this(name, false);
	}

	/**
	 * Constructs a ClientID with a unique numerical id 
	 * and a default String id.
	 * 
	 * A default is as such: "CLIENT: NUMERICAL_ID".
	 */
	public ClientID(){
		this(null);
		this.name = "CLIENT: "+id;
	}
	
	/**
	 * Constructs a ClientID with a unique numerical id or
	 * 0 if this ClientID was meant for a server
	 *
	 * @param name - the string name of this ID
	 * @param server - if this ID is meant for a server
	 */
	private ClientID(String name, boolean server) {
		if (server) {
			this.id = 0;
		}
		else {
			this.id = generateUniqueID();
		}
		this.name = name;
	}

	/**
	 * Sets the String id of this client to the given string name
	 * @param name - the given String name
	 */
	public void setName(String name){
		this.name = name;
	}

	/**
	 * Returns the numerical ID of this client
	 * @return the numerical ID of this client
	 */
	public int getID(){
		return id;
	}

	/**
	 * Returns the String ID of this client
	 * @return the string ID of this client
	 */
	public String getName(){
		return name;
	}

	/**
	 * Checks if the given Object is a ClientID and if 
	 * it's numerical id is equal to this Client's numerical ID.
	 * 
	 * @return true if it is both, false if else.
	 */
	public boolean equals(Object e){
		ClientID cId = (ClientID) e;
		return cId.id == this.id;
	}

	/**
	 * Returns the hashcode of this Object using the numerical Id of this Client
	 * @return the hashcode for this client
	 */
	public int hashCode(){
		return toString().hashCode();
	}

	/**
   * Returns the Strign representation of this ClientID
   * 
   * The format is as follows:
   * "[ID:123456] [Name:John]"
   * 
   * @return the hashcode for this client
   */
	public String toString(){
		return "[ID:"+id+"] [Name:"+name+"]";
	}

	/*
	 * Generates a unique positive integer
	 */
	public synchronized static int generateUniqueID(){
		int toGive = -1;
		Random random = new Random();

		toGive = random.nextInt();

		//The server's ID is always 0

		while (idNumsGiven.contains(toGive) == true || toGive < 0) {
			toGive = random.nextInt();
		}

		idNumsGiven.add(toGive);
		return toGive;
	}

	public synchronized static void removeGeneratedID(int idToBeRemoved){
		idNumsGiven.remove(idToBeRemoved);
	}

	public synchronized static List<Integer> allIDs(){
		return new ArrayList<>(idNumsGiven);
	}
	
	/**
	 * Returns the ClientID meant for a server
	 * 
	 * @param name - the name to be assigned to this ClientID
	 * @return the ClientID for a server
	 */
	public static ClientID getServerID(String name) {
		if (SERVER_ID == null) {
			SERVER_ID = new ClientID(name, true);
		}
		return SERVER_ID;
	}
}
