package jg.skeptik.common;

import java.io.Serializable;

/**
 * Stores the ClientID of both the server and the client.
 * 
 * A ConnectionInfo is usually sent during a Skeptik handshake from the
 * Server detailing the connecting client's ID and the server's ID, along
 * with the GOOD_BYE_STRING which is used to signal that the server
 * is shutting down or disconnecting with this client.
 * 
 * @author Jose Guaro
 *
 */
public final class ConnectionInfo implements Serializable{
	
	/**
	 * Release version is 1L
	 */
	private static final long serialVersionUID = 1L;
	public final ClientID CURRENT_CLIENT_ID;
	public final ClientID SERVER_ID;
	public final String GOOD_BYE_STRING;

	/**
	 * Constructs a ConnectionInfo with a client's ID, the server's ID
	 * and the server's GOOD_BYE_STRING
	 * 
	 * @param clientID - the ClientID of this client
	 * @param serverID - the ClientID of the server
	 * @param goodByeString - the server's GOOD_BYE_STRING
	 */
	public ConnectionInfo(ClientID clientID, ClientID serverID, String goodByeString) {
		CURRENT_CLIENT_ID = clientID;
		SERVER_ID = serverID;
		GOOD_BYE_STRING = goodByeString;
	}

}
