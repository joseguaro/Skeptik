package sampleimplmnt;

import java.nio.channels.Selector;

import jg.skeptik.server.config.ServerConfigs;
import jg.skeptik.server.logic.BasicDatabase;
import jg.skeptik.server.logic.ConnectionManager;
import jg.skeptik.server.logic.SkeptikServer;
import jg.skeptik.server.logic.engine.AbstractActionProcessor;
import jg.skeptik.server.logic.engine.AbstractSkeptikEngine;

/**
 * Opens a Skeptik Server using bare implementations. 
 * Intended as skeleton code for more complex testing. 
 * @author Jose Guaro
 *
 */
public class OpenServer {
  
  public static void main(String [] arg) throws Exception{
    SkeptikServer server = new SkeptikServer(9999, ServerConfigs.getBasicConfig());
    Selector selector = server.initialize( null);
    
    BasicDatabase database = new BasicDatabase();
    
    AbstractSkeptikEngine engine = AbstractSkeptikEngine.open(server, selector, database);
    ConnectionManager connectionManager = new ConnectionManager(server, engine, selector); 
    
    System.out.println(server.getServerAddress());
    
    engine.initialize(connectionManager);
    engine.start(AbstractActionProcessor.getDefault(server, connectionManager, engine));
  }
  
}