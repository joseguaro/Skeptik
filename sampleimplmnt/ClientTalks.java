package sampleimplmnt;

import java.io.IOException;
import java.nio.channels.Selector;


import jg.skeptik.client.logic.Connector;
import jg.skeptik.client.logic.office.AbstractMessageProcessor;
import jg.skeptik.client.logic.office.MessageProcessor;
import jg.skeptik.client.logic.office.PostOffice;
import jg.skeptik.common.Address;
import jg.skeptik.common.parcel.ParcelContent;
import jg.skeptik.server.config.ServerConfigs;
import jg.skeptik.server.logic.BasicDatabase;
import jg.skeptik.server.logic.ConnectionManager;
import jg.skeptik.server.logic.SkeptikServer;
import jg.skeptik.server.logic.engine.AbstractActionProcessor;
import jg.skeptik.server.logic.engine.AbstractSkeptikEngine;
import jg.skeptik.server.logic.engine.BasicEngineListener;

public class ClientTalks {
  
  static SkeptikServer server;
  
  static volatile int threshhold = 45;
  
  static volatile int amountDISC = 0;
  
  public static void main(String [] args) throws Exception{
    server = new SkeptikServer(9999, ServerConfigs.getBasicConfig());
    Selector selector = server.initialize(null);
    
    BasicDatabase database = new BasicDatabase();
    
    AbstractSkeptikEngine engine = AbstractSkeptikEngine.open(server, selector, database);
    engine.addListener(new BasicEngineListener());
    
    ConnectionManager connectionManager = new ConnectionManager(server, engine, selector); 
    
    engine.initialize(connectionManager);
    engine.start(AbstractActionProcessor.getDefault(server, connectionManager, engine));
    
    System.out.println("CLIENT INIT!!!!!!!!");
    
    ClientInfo client1 = new ClientInfo();
    ClientInfo client2 = new ClientInfo();
    
    client1.connect("localhost",9999);
    client2.connect("localhost",9999);
    
    new Thread(new Runnable() {

      @Override
      public void run() {
        try {
          client1.start(new MessProcess(client1.postOffice, client1.connector));
        } catch (IOException e) {
          System.out.println("IO ERROR AT CLIENT "+client1.postOffice.getConnectionInfo().CURRENT_CLIENT_ID);
          e.printStackTrace();
        }
      }
    }).start();
    
    new Thread(new Runnable() {
      
      @Override
      public void run() {
        try {
          client1.postOffice.sendObject("START DIS UP BOI", 
              client2.postOffice.getConnectionInfo().CURRENT_CLIENT_ID.getID(), 
              ParcelContent.MESSAGE);
          client2.start(new MessProcess(client2.postOffice, client2.connector));
        } catch (Exception e) {
          System.out.println("IO ERROR AT CLIENT "+client2.postOffice.getConnectionInfo().CURRENT_CLIENT_ID);
          e.printStackTrace();
        }
      }
    }).start();
    
    while (threshhold >= 0);
    
    engine.stopEngine(true);
  }
  
  public static class MessProcess extends AbstractMessageProcessor{

    public MessProcess(PostOffice postOffice, Connector connector) {
      super(postOffice, connector);
    }

    @Override
    public void processParcel(Object content, ParcelContent contentType, Address address) {
      if(address.getSender() == postOffice.getConnectionInfo().SERVER_ID.getID()) {
        System.out.println("CLIENT: Server sent '"+content.toString()+"' "+postOffice.getConnectionInfo().CURRENT_CLIENT_ID);
        if (content.equals(postOffice.getConnectionInfo().GOOD_BYE_STRING)) {
          postOffice.endTransmission(true);
        }
      }
      
      
        System.out.println("CLIENT: "+ content.toString()+" || "+threshhold+"  || TO: "+address.getRecipient());

        try {
          postOffice.sendObject("Hi, I'm ["+address.getRecipient()+"]!!", address.getSender(), ParcelContent.MESSAGE);
          threshhold--;
          Thread.sleep(250);
        } catch (Exception e) {
          e.printStackTrace();
        }
        
      
    }
    
  }
  
  
  public static class ClientInfo{
    private Connector connector;
    private PostOffice postOffice;
    
    public ClientInfo() throws IOException {
      connector = new Connector();
    }
    
    public void connect(String host, int port) throws Exception {
      connector.connect(host, port);
      System.out.println("connection successful");
      postOffice = connector.start();
      
      System.out.println("---starting handshake");
      postOffice.startSkeptikHandshake();
      System.out.println("handshake successful");
    }
    
    public void start(MessageProcessor messageProcessor) throws IOException {
      postOffice.startTransmission(messageProcessor);
    }
  }
}
