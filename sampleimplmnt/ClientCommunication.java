package sampleimplmnt;

import java.io.IOException;
import java.nio.channels.Selector;

import javax.crypto.SealedObject;

import jg.skeptik.client.logic.Connector;
import jg.skeptik.client.logic.office.AbstractMessageProcessor;
import jg.skeptik.client.logic.office.MessageProcessor;
import jg.skeptik.client.logic.office.PostOffice;
import jg.skeptik.common.Address;
import jg.skeptik.common.parcel.Parcel;
import jg.skeptik.common.parcel.ParcelContent;
import jg.skeptik.server.config.ServerConfigs;
import jg.skeptik.server.logic.BasicDatabase;
import jg.skeptik.server.logic.ConnectionManager;
import jg.skeptik.server.logic.SkeptikServer;
import jg.skeptik.server.logic.engine.AbstractActionProcessor;
import jg.skeptik.server.logic.engine.AbstractSkeptikEngine;

public class ClientCommunication {
  static long threadSleepTime = 250;
  
  static SkeptikServer skeptikServer;

  static Connector client1;
  static Connector client2;

  static int client1ID;

  static volatile int transmission = 20;
  //deducted everytime a client sends a message.
  //Once it reaches zero, the server will shutdown. 

  /*
   * Talk flow:
   * 
   * Client 2 sends a message to Client 1.
   * 
   * Client 1 then responds to Client 2 and vice versa until
   * this test has been terminated
   * 
   * Once transmission has reached 0, both clients and the server should shutdown cleanly
   */

  public static void main(String[] args) throws Exception {
    skeptikServer = new SkeptikServer(9999, ServerConfigs.getBasicConfig());
    Selector selector = skeptikServer.initialize( null);

    BasicDatabase database = new BasicDatabase();

    AbstractSkeptikEngine engine = AbstractSkeptikEngine.open(skeptikServer, selector, database);
    ConnectionManager connectionManager = new ConnectionManager(skeptikServer, engine, selector); 

    engine.initialize(connectionManager);
    engine.start(AbstractActionProcessor.getDefault(skeptikServer, connectionManager, engine));

    startClient1();
    startClient2();

    while (transmission >= 0 );
    System.out.println("-----SHUTTING DOWN SERVER------");
    engine.stopEngine(true);
  }

  private static void startClient1() throws Exception {
    client1 = new Connector();
    client1.connect("localhost", 9999);

    PostOffice postOffice = client1.start();
    postOffice.startSkeptikHandshake();

    client1ID = postOffice.getConnectionInfo().CURRENT_CLIENT_ID.getID();

    System.out.println("*****Client 1 [ID: "+postOffice.getConnectionInfo().CURRENT_CLIENT_ID+"]");

    MessageProcessor processor = new AbstractMessageProcessor( postOffice , client1) {

      @Override
      public void processParcel(Object content, ParcelContent contentType, Address address) {

        try {
          if(address.getSender() == postOffice.getConnectionInfo().SERVER_ID.getID()){
            if(content.equals(postOffice.getConnectionInfo().GOOD_BYE_STRING)){
              postOffice.endTransmission(true);
            }
          }
          System.out.println("[CLIENT:"+address.getSender()+"]: "+content.toString());

          //Just a 1/2 second pause as to not fill the console too fast
          Thread.sleep(threadSleepTime);

          transmission--;

        } catch (Exception e) {
          // TODO Auto-generated catch block
          postOffice.endTransmission(true);
        }


        try {
          postOffice.sendObject("Hello, this is Client1 ", address.getSender(), 
              ParcelContent.MESSAGE);
        } catch (Exception e) {
          e.printStackTrace();
        }


      }
    };

    new Thread(new Runnable() {

      @Override
      public void run() {
        try {
          postOffice.startTransmission(processor);
        } catch (IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    }).start();

  }

  private static void startClient2() throws Exception {
    client2 = new Connector();
    client2.connect("localhost", 9999);

    PostOffice postOffice = client2.start();
    postOffice.startSkeptikHandshake();

    System.out.println("*****Client 2 [ID: "+postOffice.getConnectionInfo().CURRENT_CLIENT_ID+"]");

    MessageProcessor processor = new AbstractMessageProcessor( postOffice , client1) {

      @Override
      public void processParcel(Object content, ParcelContent contentType, Address address) {

        try {
          if(address.getSender() == postOffice.getConnectionInfo().SERVER_ID.getID()){
            if(content.equals(postOffice.getConnectionInfo().GOOD_BYE_STRING)){
              postOffice.endTransmission(true);
            }
          }
          System.out.println("[CLIENT:"+address.getSender()+"]: "+content.toString());

          //Just a 1/2 second pause as to not fill the console too fast
          Thread.sleep(threadSleepTime);

          transmission--;

        } catch (Exception e) {
          // TODO Auto-generated catch block
          postOffice.endTransmission(true);
        }


        try {
          postOffice.sendObject("Hello, this is Client2! ", address.getSender(), 
              ParcelContent.MESSAGE);
        } catch (Exception e) {
          e.printStackTrace();
        }


      }
    };


    new Thread(new Runnable() {

      @Override
      public void run() {
        try {
          postOffice.sendObject("Hello there, I'm Client 2! ", client1ID,
              ParcelContent.MESSAGE);
          postOffice.startTransmission(processor);
        } catch (Exception e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    }).start();

  }


}
